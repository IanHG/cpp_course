#include <iostream>
#include <typeinfo> // for typeid

#include <vector>

/***** Demangle begin *******/
#include <cstdlib> // used by demangle
#include <memory>  // used by demangle
#if defined(__GNUG__)
#include <cxxabi.h> // used by demangle
#endif /* __GNUG__ */

// Typename demangling function, works only with 'gcc'/'g++'
std::string demangle(const char* name) 
{
#if defined(__GNUG__)
   int status = -4; // some arbitrary value to eliminate the compiler warning
   
   // Call __cxa_demangle
   std::unique_ptr<char, void(*)(void*)> res {
       abi::__cxa_demangle(name, NULL, NULL, &status),
       std::free
   };
   
   return (status == 0) ? res.get() : name ;
#else
   return std::string{name};
#endif /* __GNUG__ */
}
/***** Demangle end *******/

/**
 * Main function
 **/
int main()
{
   std::cout << "Some basic types " << std::endl;
   std::cout << typeid(double).name() << std::endl;
   std::cout << typeid(char).name() << std::endl;
   std::cout << typeid(int).name() << std::endl;
   std::cout << std::endl;
   
   std::cout << "Some basic types (demangled)" << std::endl;
   std::cout << demangle(typeid(double).name()) << std::endl;
   std::cout << demangle(typeid(char).name()) << std::endl;
   std::cout << demangle(typeid(int).name()) << std::endl;
   std::cout << std::endl;
   
   std::cout << "Some more advanced types " << std::endl;
   std::cout << typeid(std::vector<double>).name() << std::endl;
   std::cout << demangle(typeid(std::vector<double>).name()) << std::endl; // Write demangled name
}
