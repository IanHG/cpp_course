#include <iostream>

int i = 1;

struct R 
{
   int i;
   R(): i(2) { }
};

template<typename T>
struct S: T 
{
   void f() 
   {
      std::cout << i << ' '     // selects ::i
                << this->i      // selects R::i
                << std::endl;
   }
};

int main() 
{
   S<R>().f();
}
