#include <iostream>
#include <typeinfo>

template<class T>
class test;

template<class T>
T dot(const test<T>& t1, const test<T>& t2);

template<class T>
class test
{
   private:
      int i;
   public:
      friend T dot<>(const test& t1, const test& t2);
};

template<class T>
T dot(const test<T>& t1, const test<T>& t2)
{
   std::cout << typeid(T).name() << std::endl;
   test<int> t;
   t.i = 0;
   std::cout << " dot " << std::endl;
}


int main()
{
   test<double> t1;
   test<double> t2;
   
   dot(t1,t2);

   return 0;
}
