#include "func1.hpp"

#include <iostream>

template<class T = double>
void template_func()
{
   std::cout << " Template func in " << __FILE__ << std::endl;
}

void func1()
{
   template_func<>();
}
