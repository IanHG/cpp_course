#include "func2.hpp"

#include <iostream>

template<class T = double>
void template_func()
{
   std::cout << " Template func in " << __FILE__ << std::endl;
}

void func2()
{
   template_func<>();
}
