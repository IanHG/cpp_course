#include <iostream>

template<class T>
void func1(T t)
{
   t *= 2.0;
}

template<class T>
void func2(T& t)
{
   t *= 2.0;
}

int main()
{
   double d1 = 2.0;
   double d2 = 3.0;
   double d3 = 4.0;

   func1(d1);
   func2(d2);
   func1<double&>(d3);

   std::cout << d1 << std::endl; 
   std::cout << d2 << std::endl;
   std::cout << d3 << std::endl;

   return 0;
}
