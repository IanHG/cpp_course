\documentclass[t]{beamer}
\usetheme{CambridgeUS}

\input{header.tex}
\input{codetypeset.tex}
\input{ls.tex}


\begin{document}
\title{\cppname{} templates}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Today}

   Today we will cover:  
   \begin{itemize}
      \item
      Forward declarations
      \item
      Function templates
      \item
      Class templates
      \item
      Organizing template source code
   \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Forward declaration of functions}

   We have seen \emph{Forward declaration} for functions
   
   \includecpp[Function forward declaration]{examples/function_forward_declare/main.cpp}

   An example is also given in: 
   \begin{center}
      \iterminal{week_2/examples/square_function/program2.cpp}
   \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Forward declaration of classes}

   We can also forward declare classes

   \begin{cpp}[Forward declaration of class]
      class X; // forward declaration, I'm missing the definition of class X
   \end{cpp}
   
   \verticalspace
   \icpp{class X} is an \emph{incomplete type} until we define it! \\
   \verticalspace
   We cannot access any members of incomplete types (as they have not
   yet been declared or defined) \\
   
   \includecpplines[]{examples/class_forward_declare/main.cpp}{6}{8}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {What can we do with forward class declarations?}

   {\bf Usage 1:} \emph{Declare} functions or methods which
   accept/return incomplete types
   \includecpplines[Remember this!]{examples/forward_declare/main.cpp}{3}{7}
   
   {\bf Usage 2: } Make classes or functions
   holding/taking/returning pointers or references to incomplete types, 
   \emph{BUT} which does not access members of class.
   \includecpplines[Know this is an option]{examples/forward_declare/main.cpp}{9}{12}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Why do forward class declaration?}

   If a class in a header file does not need to know the size of a data member class or does not need to
   use the class's members, forward declare the class instead of
   including the file with \icpp{#include}. \\
   \verticalspace
   Reduce the number of \icpp{#include} files in header files, thus
   reducing unwanted coupling. (!)\\
   \verticalspace 
   It will also reduce compilation times.(!) \\
   \verticalspace
   Put include files in source code files and use forward declarations in
   header files. \\
   \verticalspace
   To avoid the need of a full class declaration, use references (or
   pointers) and forward declare the class.  \\
   \verticalspace
   We also need it later to properly implement \icpp{friend} functions for
   templated classes \\
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {The \icpp{template} keyword}

   Templates provide direct support for generic programming, \emph{i.e.}
   programming written for types \emph{to-be-specified-later} \\ 
   \verticalspace
   Used to generate multiple functions/classes from a single implementation \\
   \verticalspace
   Reduces duplication of code \\
   \verticalspace
   The STL extensively uses templates to make code as general as possible,
   hence the name Standard TEMPLATE Library \\
   \verticalspace
   \cppname{}'s template system is proven Turing Complete and can be used
   for so-called meta-programming (we will probably re-visit this aspect
   at a later date) \\
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Declaring a \icpp{template} function}

   Use keywords \icpp{typename} or \icpp{class} to declare template
   parameters \\
   \mediumverticalspace
   The two are equivalent, I'll will exclusively use \icpp{class} ! \\
   \mediumverticalspace
   Uses angle brackets \icpp{<} and \icpp{>} for declarations, always in pairs \\
   \mediumverticalspace
   At first use the compiler will generate code for the function, this is
   called \emph{template instantiation}
   \begin{cpp}[These three declarations are equivalent]
      template<class T>
      void function(T t); // I will exclusive use this form
      // ...
      template<typename T>
      void function(T t); // but this is completely equivalent...
      // ...
      template<class T> void function(T t); // can also be done as 1-liner
   \end{cpp}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function overloads
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Not using templates (instead we overload)}

   \includecpplines[Tedious overloads]{examples/overloads_vs_templates/overload.cpp}{3}{11}
   \includecpplines[Usage]{examples/overloads_vs_templates/overload.cpp}{26}{30}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function templates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {With \icpp{template}'s}

   \includecpplines[\icpp{template}'d function]{examples/overloads_vs_templates/template.cpp}{3}{7}
   \includecpplines[Usage is the same]{examples/overloads_vs_templates/template.cpp}{11}{17}

   We have reduced the number of lines of code! (less complexity)\\
   \verticalspace
   If we want to change \icpp{print_times_two()}, we only change in one place
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Multiple template parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Multiple template parameters}

   We can have more than one template parameter \\
   \includecpp[Quiz]{examples/multiple_template_parameters/main.cpp}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vector
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\icpp{template} classes}

   We already know a class \icpp{template}, namely the \icpp{vector} class if the STL \\
   \verticalspace
   \icpp{vector<double>} will give us a vector of \icpp{double}'s \\
   \verticalspace
   \icpp{vector<int>} will declare a vector of integers, etc. \\
   \begin{cpp}[A simple templated vector implementation]
   template<class T>
   class vector
   {
      private:
         size_t m_size;     // size_t is equivalent to an unsigned integer
         T*     m_elements; // pointer to T's

         // ... some more code ...
   };
   \end{cpp}

   \includecpplines[Usage]{examples/vector/main.cpp}{7}{7}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vector continued
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Member declarations}
  
   \begin{cpp}[Declaration]
      template<class T>
      class vector
      {
         public:
            vector(std::size_t i); // declare constructor from size
      };
   \end{cpp}

   \includecpplines[Implementation]{examples/vector/vector.hpp}{95}{99}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Friend declarations (the correct way...)}
   
   \begin{cpp}[Making \icpp{friend}'s the 'correct' way]
      template<class T>
      class vector; // forward declare vector class 
      
      template<class T>
      T dot(const vector<T>&, const vector<T>&); //forward declare dot function on vector
      
      template<class T>
      class vector
      {
        public:
          friend T dot<>(const vector<T>& v1, const vector<T>& v2);// dot is a friend of vector 
                                                                   // i need <> to tell the
                                                                   // compiler that dot is templated
      };
      
      template<class T>
      T dot(const vector<T>& t1, const vector<T>& t2)
      {
        // ... put implementation here ...
      }
   \end{cpp}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Distrinct types
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Different instantiations are distinct types!}
   
   \icpp{vector<double>} and \icpp{vector<int>} are distinct/different types \\
   \verticalspace

   \includecpplines[]{examples/vector/main.cpp}{36}{43}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Class \icpp{template} specializations}

   We can specialize a class template to have specific
   behaviour for specific types \\
   \verticalspace
   The most specialized class that fits will get used! \\
   \begin{cpp}
      template<class T>
      class vector;
      
      template<>
      class vector<int> // specialize class template for int types
      {
        // ... implement special code for vector of integers
      };
      
      // some code 
      vector<double> d_vec; // will use general implementation
      
      vector<int> i_vec; // will use specialization for integer
   \end{cpp}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Function \icpp{template} specializations}

   We can also specialize function templates \\
   \verticalspace
   Again the best fit will get chosen \\
   \begin{cpp}
      // general declaration for the dot function
      template<class T>
      T dot(const vector<T>& v1, const vector<T>& v2);

      // specialization for int's
      template<>
      int dot(const vector<int>& v1, const vector<int>& v2);
      
      // specialization for vector of complex numbers...
      template<class T>
      complex<T> dot(const vector<complex<T> >& v1, const vector<complex<T> >& v2);
   \end{cpp}
   
   \end{frame}

%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%
%%\begin{frame}[fragile]{Partial specializations}
%%  partial specializations
%%\end{frame}
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Source code organization using templates}

   Code for templated classes and functions is only generated when they are
   instantiated \\
   \verticalspace
   This means that all templated code used must be included in ALL
   compilation unit where they are used! \\
   \verticalspace
   We thus have two options for organizing our source code: \\
   \smallverticalspace
   
   \begin{itemize}
      \item
      Include template definitions before their use in a translation unit \\
      
      \item
      Include template declarations before their use in a translation unit.
      Afterwards include definitions (Potentially after use).
   \end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Source code organization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Example 1}

   \includecpplines[Definition]{examples/source_code_organization/option_1/out.hpp}{5}{11}
   \includecpp[main()]{examples/source_code_organization/option_1/main.cpp}

   Go check \iterminal{examples/source_code_organization/option_2} yourself!
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\icpp{template}'s pros and cons}
   
   Pros:
   \begin{itemize}
      \item
         Less code duplication
      \item
         Easier to maintain
   \end{itemize}

   Cons:
   \begin{itemize}
      \item
         Templates must be included everywhere they are used, which gives slower compilation times
      \item
         Can be hard to debug
      \item
         You need to be careful when linking (see exercises)
   \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Done for today!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  {Finally he's done talking...}

   \begin{center}
      \large Let's go write some code!
   \end{center}
\end{frame}

\end{document}
