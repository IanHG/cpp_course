/**
 * NB! NB! NB! This example will NOT compile, but is here to showcase the compiler error when trying to use an incomplete type.
 **/
int main()
{
   class X;    // forward declare X
   X().getx(); // X is an incomplete type, this will NOT compile!

   // Even if i define the class later it will still not compile as Y is still incomplete when used.
   class Y;
   Y().gety(); // This will NOT compile
   class Y
   {
      int gety() const { return 1; };
   };

   return 0;
}
