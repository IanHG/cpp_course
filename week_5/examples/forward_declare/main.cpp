#include <iostream>

class X; // forward declare class X
   
void f1(const X&); // I only declare function... no definitiion
X f2();
// I can now call functions f1 and f2... still need to make definition later though

void func(X* x)
{
   f1(*x); // use f1 and reference to X without definitions yet
}

// Define class X
class X
{
   public:
      int i;
};

// Define f1
void f1(const X& x)
{
   std::cout << x.i << std::endl;
}

// Define f2
X f2()
{
   X x;
   x.i = 4;
   return x;
}

int main()
{
   X x = f2();

   func(&x);

   return 0;
}
