#include <iostream>

template<class T> // template function to take 'any' type
void print_times_two(T t)
{
   std::cout << 2 * t << std::endl; // type must overload operator* and operator<<
}

int main()
{
   int i = 3;
   print_times_two(i); // compiler will generate and call an integer version
   
   double d = 2.0;
   print_times_two(d); // compiler will generate and call a double version
   
   print_times_two<double>(i); // I can also force the double version 
  
   // These also work automatically
   short s = 7;
   print_times_two(s); // will call short version
   
   float f = 2.1;
   print_times_two(f); // will call double version

   return 0;
}
