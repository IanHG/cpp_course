#include <iostream>

void print_times_two(int i)
{
   std::cout << 2 * i << std::endl;
}

void print_times_two(double d)
{
   std::cout << 2 * d << std::endl;
}

// ... more overloads for other types...
void print_times_two(short s)
{
   std::cout << 2 * s << std::endl;
}

void print_times_two(float f)
{
   std::cout << 2 * f << std::endl;
}

int main()
{
   int i = 3;
   print_times_two(i); // will call integer version
   
   double d = 2.0;
   print_times_two(d); // will call double version
   
   short s = 7;
   print_times_two(s); // will call short version
   
   float f = 2.1;
   print_times_two(f); // will call double version

   return 0;
}
