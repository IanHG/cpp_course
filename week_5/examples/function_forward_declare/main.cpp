void func(); // forward declare, we give no definition

int main()
{
  func(); // call the function, we have still not defined it
}

void func() // afterwards we define function
{
  // ... give implementation ...
}
