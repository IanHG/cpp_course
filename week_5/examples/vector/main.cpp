#include <iostream>

#include "vector.hpp"

int main()
{
   vector<double> vec(10); // declare a vector of 10 double's
   
   // print size
   std::cout << vec.size() << std::endl;
   
   // Loop over all elements
   for(int i = 0; i < vec.size(); ++i)
   {
      vec[i] = double(i);
   }

   // Print all elemnts
   for(int i = 0; i < vec.size(); ++i)
   {
      std::cout << vec[i] << " ";
   }
   std::cout << std::endl;

   // create different types of vectors
   vector<int>  ivec;
   vector<bool> bvec;

   // We also create a vector of more complex types
   vector<vector<double> > simple_matrix; // very simple matrix

   // Calclating the dot product 
   double dot_result = dot(vec, vec);
   std::cout << dot_result << std::endl;

   // Cannot do dots between vectors of different types, as we only allowed same type in both vectors
   vector<double> d_dot_vec;
   vector<int>    i_dot_vec;
   
   //!!!!! The below statement will not compile. !!!!!
   //dot(d_dot_vec, i_dot_vec); // d_dot_vec and i_dot_vec are different types. 
                                // This will NOT compile!
                                // remember dot was declared for two vector<T>'s
   
   
   dot(i_dot_vec, i_dot_vec); // will call special integer dot

   vector<std::complex<double> > complex_d_dot_vec;
   dot(complex_d_dot_vec, complex_d_dot_vec); // will call special complex<T> dot
   
   return 0;
}
