#pragma once
#ifndef VECTOR_HPP_INCLUDED
#define VECTOR_HPP_INCLUDED

#include <cstddef> // for std::size_t
#include <iostream>
#include <cstdlib> // std::exit

/**
 * Note on std::size_t:
 *
 * std::size_t in most compilers equivalent to an unsigned int.
 *
 * For very technical reasons it is actually better to use an 'int',
 * but the 'std::vector' from STL uses std::size_t, and this example is
 * meant to be a (VERY) simple mock-up of that class.
 *
 * The reason an 'int' is better, is that for many looping control-structure (e.g. 'for'),
 * the compiler can do more optimizations when looping an 'int' than an 'unsigned int',
 * as overcounting an 'int' is undefined behaviour whereas overcounting an 'unsinged int' is OK!
 * Bjarne Stroustrup (the creator of C++=), has himself said, 
 * that using std::size_t for std::vector was a mistake.
 **/

template<class T>
class vector; // forward declare vector class 

template<class T>
T dot(const vector<T>&, const vector<T>&); //forward declare dot function on vector
                                           // we only allow dots for vectors of the same type

/**
 * Mock-up of STL vector
 **/
template<class T>
class vector
{
   private:
      std::size_t m_size;     // size_t is equivalent to an unsigned integer
      T*          m_elements; // pointer to T's

      void allocate()
      {
         m_elements = new T[m_size];
      }

      void deallocate()
      {
         if(m_elements)
         {
            delete[] m_elements;
         }
      }
   public:
      vector(size_t size = 0); // size defaulted to 0

      ~vector();

      T& operator[](int i);
      const T& operator[](int i) const;

      size_t size() const;

      friend T dot<>(const vector& v1, const vector& v2);// dot is a friend of vector 
                                                        // i need <> to tell the
                                                        // compiler that dot is templated
};

template<class T>
vector<T>::vector(size_t size)
   :  m_size(size)
   ,  m_elements(nullptr)
{
   allocate();
}

template<class T>
vector<T>::~vector()
{
   deallocate();
}

template<class T>
T& vector<T>::operator[](int i)
{
   return m_elements[i];
}

template<class T>
const T& vector<T>::operator[](int i) const
{
   return m_elements[i];
}

template<class T>
size_t vector<T>::size() const
{
   return m_size;
}

template<class T>
T dot(const vector<T>& v1, const vector<T>& v2)
{
   T dot = T(0);
   if(v1.size() == v2.size())
   {
      for(int i = 0; i < v1.size(); ++i)
      {
         dot += v1[i] * v2[i];
      }
   }
   else
   {
      std::cout << " size not the same in dot(...). Exiting." << std::endl;
      std::exit(1);
   }
   return dot;
}

template<>
int dot(const vector<int>& v1, const vector<int>& v2)
{
   std::cout << " Calling special integer dot function ! " << std::endl;
   int dot = int(0);
   if(v1.size() == v2.size())
   {
      for(int i = 0; i < v1.size(); ++i)
      {
         dot += v1[i] * v2[i];
      }
   }
   else
   {
      std::cout << " size not the same in dot(...). Exiting." << std::endl;
      std::exit(1);
   }
   return dot;
}

#include <complex> // for specialization of dot for std::complex

template<class T>
std::complex<T> dot(const vector<std::complex<T> >& v1, const vector<std::complex<T> >& v2)
{
   std::cout << " Calling special complex<T> dot function ! " << std::endl;
   std::complex<T> dot = std::complex<T>(0);
   if(v1.size() == v2.size())
   {
      for(int i = 0; i < v1.size(); ++i)
      {
         dot += v1[i] * std::conj(v2[i]);
      }
   }
   else
   {
      std::cout << " size not the same in dot(...). Exiting." << std::endl;
      std::exit(1);
   }
   return dot;
}

#endif /* VECTOR_HPP_INCLUDED */
