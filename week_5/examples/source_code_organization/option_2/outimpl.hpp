#pragma once
#ifndef OUT_IMPL_HPP_INCLUDED
#define OUT_IMPL_HPP_INCLUDED

#include <iostream>

template<class T> 
void out(const T& t) // declare and define template
{
   std::cout << t << std::endl;
}

#endif /* OUT_IMPL_HPP_INCLUDED */
