#include "outdecl.hpp" // include header with declaration

int main()
{
   out(2.0);
   return 0;
}

#include "outimpl.hpp" // include file with definitions
