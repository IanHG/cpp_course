#pragma once
#ifndef OUT_DECL_HPP_INCLUDED
#define OUT_DECL_HPP_INCLUDED

template<class T> 
void out(const T& t); // declare template

#endif /* OUT_DECL_HPP_INCLUDED */
