#include <iostream>

template<class T, class U> // give template parameters as comma separated list
T add(T t, U u) //add two objects of different type and return result as first type
{
   return t + u;
}

int main()
{
   double d = 2.1;
   int i = 3;

   std::cout << add(d,i) << std::endl; // what will get printed??
   std::cout << add(i,d) << std::endl; // what will get printed??

   return 0;
}
