#include <iostream>
#include <functional> // std::bind

int main()
{
   // 
   auto f2 = std::bind([](int i, int j){std::cout << (i+j) << std::endl;}
                     , std::placeholders::_1 // first arg to f is bound to i
                     , 3  // bind 3 to j
                     );
   // f2 is of type std::function<void(int)>
   // i.e. a function taking an integer returning nothing 
   f2(2); // print 5 (= 2+3)
}
