#include <iostream>
#include <functional> // std::bind

int main() {
   // bind 1 to first arg
   auto f = std::bind([](int i){std::cout << i << std::endl;},1);
   // f is of type std::function<void()>,
   // i.e. a function taking no arguments and return nothing
   f(); // print 1
   
}
