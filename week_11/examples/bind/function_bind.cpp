#include <iostream>
#include <functional>

int subtract(int i, int j) { return i-j;}

int main() {
   // we can use bind on a function pointer
   auto f1 = std::bind(subtract,std::placeholders::_1,std::placeholders::_2);
   std::cout << f1(1,2) << std::endl; // print -1 
   
   // also we can swap argument order with placeholders
   auto f2 = std::bind(subtract,std::placeholders::_2,std::placeholders::_1);
   std::cout << f2(1,2) << std::endl; // print 1... why ?!

   return 0;
}
