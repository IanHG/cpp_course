#include <iostream>
#include <functional>

void func(std::function<void(int)> f) { f(8); }

int main() {
   int i;
   auto f = [&i](int j) mutable { i = j; }; // must be mutable to change i
   //auto f = [&i](int j) { i = j; }; // will not compile
   f(3);
   std::cout << i << std::endl; // print 3
   func(f);
   std::cout << i << std::endl; // print 8
}
