#include <iostream>
#include <functional> // std::function
#include "../functor_addition/adder.h"

void func(std::function<int(int)> f, int i) { 
   // ... do some stuff ...
   i=f(i); // do callback
   std::cout << i << std::endl;
}

int add_two(int i) { return i + 2; }

int main() {
   func([](int i){ return i + 1; }, 1); // call with lambda function
   func(add_two,1); // call with function pointer
   func(adder(3),1); // call with functor
}
