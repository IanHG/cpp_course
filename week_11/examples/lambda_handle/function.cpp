#include <iostream>
#include <functional> // for std::function

int main() {
   std::function<int(int)> f = [](int i){return i+1;}; // bind lambda to f
   std::cout << f(2) << std::endl; // call lambda and print 3
}
