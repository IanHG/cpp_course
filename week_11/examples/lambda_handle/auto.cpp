#include <iostream>
#include <functional> // for std::function

int main() {
   auto f = [](int i){return i+1;}; // compiler wil automatically deduce type of f
   std::cout << f(2) << std::endl; // call lambda and print 3
}
