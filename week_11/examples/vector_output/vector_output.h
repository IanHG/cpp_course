#ifndef VECTOR_OUTPUT_H_INCLUDED
#define VECTOR_OUTPUT_H_INCLUDED

#include <iostream>
#include <vector>

template<class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
   os << "(";
   if(vec.size() > 0)
   {
      os << vec[0];
      for(size_t i = 1; i < vec.size(); ++i)
      {
         os << "," << vec[i];
      }
   }
   os << ")";
   return os;
}

#endif /* VECTOR_OUTPUT_H_INCLUDED */
