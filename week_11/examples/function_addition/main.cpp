#include <algorithm> // std::transform
#include "../vector_output/vector_output.h" // std::vector + std::cout

double add_two(double d) { return d + 2.0; } // function for adding 2

double add_three(double d) { return d + 3.0; } // function for adding 3

int main() {
   std::vector<double> v = {1.0,2.0,3.0};
   
   std::vector<double> v2(3);
   std::transform(v.begin(),v.end(),v2.begin(),add_two);
   std::cout << v2 << std::endl; // prints: (3,4,5)
   
   std::vector<double> v3(3);
   std::transform(v.begin(),v.end(),v3.begin(),add_three);
   std::cout << v3 << std::endl; // prints: (4,5,6)
}
