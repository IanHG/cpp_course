#ifndef ADDER_H_INCLUDED
#define ADDER_H_INCLUDED

class adder {
   double add; // our adder functor holds a double (it has state)
   public:
      adder(double d): add(d) { } // define c-tor from double
      
      double operator()(double d) const { // define operator()
         return d + add; // return result of invoking functor
      }
};

#endif /* ADDER_H_INCLUDED */
