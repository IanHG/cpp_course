#include <vector>
#include <algorithm> // for std::transform
#include "../vector_output/vector_output.h" // define operator<< for std::vector

#include "adder.h"

int main() {
   std::vector<double> v = {1.0,2.0,3.0};
   
   std::vector<double> v2(3);
   std::transform(v.begin(),v.end(),v2.begin(),adder(2)); // pass adder object
   std::cout << v2 << std::endl; // prints: (3,4,5)
   
   std::vector<double> v3(3);
   std::transform(v.begin(),v.end(),v3.begin(),adder(3)); // pass adder object
   std::cout << v3 << std::endl; // prints: (4,5,6)
}
