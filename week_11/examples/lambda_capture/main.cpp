#include <algorithm>
#include "../vector_output/vector_output.h"

int main() {
   std::vector<double> v = {1.0,2.0,3.0};
   std::vector<double> v2(3);

   double add = 2.0; // init add to 2
   std::transform(v.begin(),v.end(),v2.begin(),[add](double d){return d+add;});
   std::cout << v2 << std::endl; // print: (3,4,5)
   
   add = 3.0; // set add to 3 now
   std::transform(v.begin(),v.end(),v2.begin(),[add](double d){return d+add;});
   std::cout << v2 << std::endl; // print: (4,5,6)

   return 0;
}
