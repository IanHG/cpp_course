#include <algorithm>
#include "../vector_output/vector_output.h"

int main() {
   std::vector<double> v = {1.0,2.0,3.0};

   std::vector<double> v2(3);
   std::transform(v.begin(),v.end(),v2.begin(),[](double d){ return d+2.0;} );
   std::cout << v2 << std::endl; // print: (3,4,5)

   return 0;
}
