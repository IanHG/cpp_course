#include <vector>
#include <functional>
#include <iostream>

void callfunctions(const std::vector<std::function<void()> >& vec_func)
{
   for(auto iter = vec_func.begin(); iter!=vec_func.end(); ++iter)
   {
      (*iter)();
   }
}


int main() 
{
   std::vector<std::function<void()> > vec;
   
   vec.push_back([](){std::cout << "H";});
   
   callfunctions(vec);
   
   return 0;
}
