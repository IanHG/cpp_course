#include <iostream>

class slowpoke {
   public:
      slowpoke() = default;
      slowpoke(const slowpoke&) = delete;
      void speak() { std::cout << "slooowpoooke, slooow" << std::endl;  }
};

int main()
{
   slowpoke slow;
   auto f = [slow]() {slow.speak(); };
   f();

   return 0;
}
