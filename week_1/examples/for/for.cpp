#include <iostream>

int main()
{
   int sum = 0; // initialize sum to 0
   for(int i = 0; i < 10; ++i) // loop over i
   {
     sum += i;
   }
   std::cout << "Sum:" << sum << std::endl; // what is the sum?

   return 0;
}
