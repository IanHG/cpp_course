#include <iostream>   // std::cout, std::endl
#include <cmath>      // std::ceil

int main()
{
   double x; // declare variable x as double
   x = 2.0;  // assign value 2.0 to x (x <- 2.0)
   double pi = 3.14; // declare and assign variable pi 
   int variablenamescanbeaslongaswewantbutmaybethisisoverdoingit;
   
   int i = 1;
   double di = i; // integer is converted double and assigned to di
   
   double dj = 2.1;
   int j = dj; // rounded down (floored) to nearest integer (j <- 2)

   double dk = 2.8;
   int k = dk;             // Round down (floored), can also be done with std::floor(...)
   int k2 = std::ceil(dk); // Round up (ceiled)

   std::cout << x  << std::endl;
   std::cout << pi << std::endl;
   std::cout << variablenamescanbeaslongaswewantbutmaybethisisoverdoingit << std::endl; // Not initialized, will print rubbish
   
   std::cout << di    << std::endl;
   std::cout << i     << std::endl;
   
   std::cout << di    << std::endl;
   std::cout << dj << std::endl;
   std::cout << j  << std::endl;
   
   std::cout << dk << std::endl;
   std::cout << k  << std::endl;
   std::cout << k2 << std::endl;

   return 0;
}
