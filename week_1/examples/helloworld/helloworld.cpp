#include <iostream> // include c++ IO routines

using namespace std; // put std namespace into global scope

int main() // we must have a main function
{
   cout << "Hello World!" << endl; // print to terminal
   return 0; // return exit code 0 to shell
}
