#include <iostream> 
#include <string> // for c++'s string class

using namespace std; 

int main() 
{
   string s; // declare variable s as a string
   cout << "Please enter your name:" << endl;
   cin >> s; // read input into s
   cout << "Hello World! " << s << endl; // say hello to user
   return 0; 
}
