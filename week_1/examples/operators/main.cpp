#include <iostream>

int main()
{
   double x = 2.0; // assign the value of 2.0 to x
   x = x + 2.0;     // assign to x the value of 2.0 added to x (x <- x + 2.0)
   x = x*2.0;       // multiply x with 2.0 and assign the result to x
   x *= 2.0;        // same as the above (this is efficient shorthand)
   double y = 4.0; // assign another variable
   double z = x/y; // divide variable x with variable y, assign result to z
   
   std::cout << z << endl; // what will get printed? 
}
