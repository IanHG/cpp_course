#include <iostream>

int main()
{
   int i = 3, j = 2;
   if(j) // we can also check an int directly
   {
     i = 10;
   }
   std::cout << i << std::endl; // what is i?

   return 0;
}
