#include <iostream>

int main()
{
   int i = 7, j = 2;
   if(j == 3) // Check, is j equal to 3
   {
     i = 10;
   }
   else
   {
     i = 2;
   }
   std::cout << i << std::endl; // what is i?

   return 0;
}
