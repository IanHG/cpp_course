#include<vector> // include source code for the vector class
using std::vector; // put vector class in global namespace

int main()
{
  vector<int> ivec(3); // declare a vector of int's of size 3
  vector<double> vec; // declare a vectors of double's of size 0

  for(int i = 0; i < ivec.size(); ++i) // loop over size of vector
  {
    ivec[i] = i+1; // access element i and set its value to i+1
                 // what is the index of the first element (what is the
                 // value of i when accessing the first element) ?? 
  }
  return 0;
}
