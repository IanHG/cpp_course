#include <iostream>

/**
 * ! NB ! NB ! NB ! This program will not compile!! ! NB ! NB ! NB !
 *
 * Try to compile it anyways, and see if you can figure out the error based on
 * the compilers output.
 **/
int main()
{
   { // start a scope
      // in the new scope i have indented all the code for easy overview
      double x = 2.0; // declare variable in outer scope

      { // start a new scope
        // started new scope so I make another level of indentation
        double y; // declare variable in inner scope
        y = x;  //  ok: both x and y are available in this scope
      } // end the new scope, y is destroyed

      std::cout << x << endl; // ok: x is available in this scope
      std::cout << y << endl; // NOT OK: y is unavailable in this scope
                             // => COMPILATION ERROR
   } // end scope 
}
