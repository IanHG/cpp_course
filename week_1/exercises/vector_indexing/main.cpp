#include <vector>

using std::vector;

int main()
{
   vector<double> vec(5);
   double x = 1.618;

   for(int i = 0; i < vec.size(); ++i)
   {
      vec[i] = x*i;
   }

   return 0;
}
