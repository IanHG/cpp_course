#include<iostream>

int main()
{
   int i=1;
   int fac=1;

   while(i<10)
   {
      fac=fac*i;
      ++i;
   }

   std::cout << fac << std::endl; // I have not put std into global namespace
                                  // must prepend cout and endl with std::
   return 0;
}
