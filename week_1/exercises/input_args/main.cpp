#include <iostream>

/**
 * We can also take command line arguments
 *
 * 'nargs' is the number of arguments
 * 'vargs' is an array arguments in pointer to char format
 *
 * First argument (i.e. index 0) is always the name of the executeable itself.
 **/
int main(int nargs, char* vargs[])
{
   // Print each argument on separate lines
   for(int i = 0; i < nargs; ++i)
   {
      std::cout << vargs[i] << std::endl;
   }

   return 0;
}
