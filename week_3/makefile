# LaTeX Makefile v1.0 -- LaTeX + PDF figures + PNG figures

ALL=$(wildcard *.tex figures/*/*-figure0.pdf)
MAIN=$(wildcard lecture_*.tex)
LATEX=pdflatex
SHELL=/bin/bash

TEXINPUTS:=../headers:../../../headers:../sty:${TEXINPUTS}
export TEXINPUTS

HEADERS_TEX=$(wildcard ../headers/*.tex)
FIGURES_TEX=$(wildcard figures/*/*.tex)
FIGURES_PDF=$(subst .tex/,-figure0.pdf/,$(FIGURES_TEX:.tex=-figure0.pdf))
FIGURES=$(subst .tex/,/,$(FIGURES_TEX:.tex=))

all: $(FIGURES_PDF) $(MAIN) $(HEADERS_TEX) ## Build lecture
	$(LATEX) $(MAIN)        		# main run
	#bibtex $(MAIN:.tex=)         # bibliography
	#makeglossaries $(MAIN:.tex=) # list of abbreviations, nomenclature
	$(LATEX) $(MAIN)              # incremental run

clean:  ## Clean LaTeX and output figure files
	rm -f *.aux 
	rm -f *.bbl 
	rm -f *.log 
	rm -f *.out 
	rm -f *.snm 
	rm -f *.toc 
	rm -f *.blg 
	rm -f *.nav 
	rm -f *.vrb
	rm -f *.auxlock
	rm -f *.md5
	rm -f $(FIGURES_PDF)

%-figure0.pdf: %.tex $(HEADERS_TEX) ## Build figures for the lecture
	cd $(dir $*) && rm -f $(notdir $@) && pdflatex --shell-escape $(notdir $<)

watch:  ## Recompile on any update of LaTeX or SVG sources
	@while true; do           \
		inotifywait $(ALL);    \
		sleep 0.01;            \
		make all;              \
		echo "\n----------\n"; \
		done

help:  # http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@grep -P '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
.DEFAULT_GOAL := all
.PHONY: help
