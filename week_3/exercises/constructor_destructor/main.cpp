#include <iostream>

class Tester
{
   int i_;

   public:
      Tester(): i_(0)
      {
         std::cout << " Default constructor: " << i_ << std::endl;
      }

      Tester(const Tester& othertester): i_(othertester.i_)
      {
         std::cout << " Copy constructor: " << i_ << std::endl;
      }

      Tester(int i): i_(i)
      {
         std::cout << " Integer constructor: " << i_ << std::endl;
      }

      ~Tester()
      {
         std::cout << " Destructor: " << i_ << std::endl;
      }

      int GetI() const
      {
         return i_;
      }
};

void func1(Tester tester)
{
   std::cout << " func1, pass by value: " << tester.GetI() << std::endl;
}

void func2(const Tester& tester)
{
   std::cout << " func2, pass by const reference: " << tester.GetI() << std::endl;
}

void func3(Tester* ptester)
{
   std::cout << " func3, pass by pointer: " << ptester->GetI() << std::endl;
}

int main()
{  
   Tester tester1;
   Tester tester2(3);
   Tester tester3(tester2);
   
   func1(tester1);
   func2(tester2);
   func3(&tester3);

   return 0;
}
