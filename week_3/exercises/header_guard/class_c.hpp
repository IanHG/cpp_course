#pragma once
#ifndef CLASS_C_HPP_INCLUDED
#define CLASS_C_HPP_INCLUDED

#include "class_a.hpp"
#include "class_b.hpp"

class class_c
{
   class_a first;
   class_b second;
};

#endif /* CLASS_C_HPP_INCLUDED */
