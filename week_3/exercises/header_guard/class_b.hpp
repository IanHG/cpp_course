#pragma once
#ifndef CLASS_B_HPP_INCLUDED
#define CLASS_B_HPP_INCLUDED

#include "class_a.hpp"

class class_b
{
   class_a first;
   class_a second;
};

#endif /* CLASS_B_HPP_INCLUDED */
