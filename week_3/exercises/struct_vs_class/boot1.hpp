#pragma once
#ifndef BOOT1_HPP_INCLUDED
#define BOOT1_HPP_INCLUDED

struct boot1_struct
{
   double m_size;
   double m_heelheight;
};

class boot1_class
{
   double m_size;
   double m_heelheight;
};

#endif /* BOOT1_HPP_INCLUDED */
