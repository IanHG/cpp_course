#pragma once
#ifndef BOOT2_HPP_INCLUDED
#define BOOT2_HPP_INCLUDED

struct boot2_struct
{
   double m_size;
   double m_heelheight;
};

class boot2_class
{
   public:
      double m_size;
      double m_heelheight;
};

#endif /* BOOT2_HPP_INCLUDED */
