#pragma once
#ifndef TABLE_HPP_INCLUDED
#define TABLE_HPP_INCLUDED

struct table_struct1
{
      void fold() {};
   private:
      int m_numLegs;
      double m_topArea;
};

struct table_struct2
{
   private:
      int m_numLegs;
      double m_topArea;
   public:
      void fold() {};
};

class table_class
{
   public:
      void fold() {};
   private:
      int m_numLegs;
      double m_topArea;
};

#endif /* TABLE_HPP_INCLUDED */
