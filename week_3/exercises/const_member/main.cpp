#include <iostream>

class X
{
   private:
      int x_;
   
   public:
      X(int x): x_(x)
      {
      }

      int Getx()
      {
         return x_;
      }
};

void printx(const X& x)
{
   std::cout << x.Getx() << std::endl;
}


int main()
{
   X x(2);
   printx(x);
   return 0;
}
