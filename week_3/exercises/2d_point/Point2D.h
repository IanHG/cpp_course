class Point2D
{
   private:
      double m_x;
      double m_y;

   public:
      Point2D(): m_x(0.0), m_y(0.0) // default origo
      {
      }

      Point2D(double x, double y): m_x(x), m_y(y)
      {
      }
};
