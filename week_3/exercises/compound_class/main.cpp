#include <iostream>

class class_a
{
   public:
      class_a()
      {
         std::cout << " Constructing : class_a " << std::endl;
      }

      ~class_a()
      {
         std::cout << " Destroying : class_a " << std::endl;
      }
};

class class_b
{
   public:
      class_b()
      {
         std::cout << " Constructing : class_b " << std::endl;
      }

      ~class_b()
      {
         std::cout << " Destroying : class_b " << std::endl;
      }
};

class class_c
{
   class_a a;
   class_b b;

   public:
      class_c()
         :  a()
         ,  b()
      {
         std::cout << " Constructing : class_c " << std::endl;
      }

      ~class_c()
      {
         std::cout << " Destroying : class_c " << std::endl;
      }
};

class class_d
{
   class_a a;
   class_b b;

   public:
      class_d()
         :  b()
         ,  a()
      {
         std::cout << " Constructing : class_d " << std::endl;
      }

      ~class_d()
      {
         std::cout << " Destroying : class_d " << std::endl;
      }
};

int main()
{
   class_c c;
   {
      class_c c2;
   }
   class_d d;

   return 0;
}

