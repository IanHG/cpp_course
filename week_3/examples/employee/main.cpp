#include <iostream>
#include "Employee.hpp"

int main()
{
   // Declare some Employee's 
   Employee gs;      // default construction of employee
   Employee da(gs);  // copy construction
   Employee oc("Ove", 50, 100); // create an employee
   Employee ig("Ian", 33, 10000000000); // create another employee (with a high salary!)
   
   // Showcasing destructors
   { // start scope
      Employee employee; // declare employee as type Employee
   } // employee goes out of scope and its destructor gets called

   // Do some print-outs
   std::cout << ig.Age() << std::endl;
   std::cout << (da == ig) << std::endl;
   std::cout << (da == gs) << std::endl;

   // Give raise
   gs.GiveRaise();

   // Do some more print-outs
   std::cout << (da == gs) << std::endl;

   return 0;
}
