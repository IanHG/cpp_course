#include "Employee.hpp" // Include header file

/**
 * Default constructor
 **/
Employee::Employee()
{
}

/**
 * Copy constructor.
 *
 * Copy another employee, by initializing each element to value of the copied object.
 **/
Employee::Employee(const Employee& other)
   :  m_name  (other.m_name)    // Initialize m_name from other employee name
   ,  m_age   (other.m_age)     // Initialize m_age from other employee age
   ,  m_salary(other.m_salary)  // Initialize m_salary from other employee salary
{
}

/**
 * Custom construtor.
 *
 * Construct Employee from string, int, and double.
 **/
Employee::Employee(const std::string& name, int age, double salary)
   :  m_name  (name)    // Initialize m_name from argument name
   ,  m_age   (age)     // Initialize m_age from argument age
   ,  m_salary(salary)  // Initialize m_salary from argument salary
{
}

/**
 * Destructor.
 *
 * All members are default destruct-able, so we do not have to do anything here.
 **/
Employee::~Employee()
{
}

/**
 * Member function GiveRaise
 **/
void Employee::GiveRaise()
{
   m_salary *= 10;
}

/**
 * Member function GiveRaise
 **/
int Employee::Age() const
{
   return m_age;
}

/**
 * Friend function operator==
 *
 * Check if two Employee's are the same.
 * This is done by checking that each member of the two Employee's are equal.
 **/
bool operator==(const Employee& e1, const Employee& e2)
{
   return e1.m_name   == e2.m_name
       && e1.m_age    == e2.m_age
       && e1.m_salary == e2.m_salary;
}
