#include <iostream>

// Define struct of type ''
struct point2d
{
   double x = 0.0; // Declare double x, default value to 0.0
   double y = 0.0; // Declare double y, default value to 0.0
}; // End struct definition with ';' (semi-colon)

int main()
{
   point2d p2d; // Declare variable of type point2d

   std::cout << "x: " << p2d.x << "   y: " << p2d.y << std::endl;

   p2d.x = 2.0;
   p2d.y = 3.0;
   
   std::cout << "x: " << p2d.x << "   y: " << p2d.y << std::endl;

   return 0;
}
