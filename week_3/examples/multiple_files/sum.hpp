#pragma once  // Header guard, make sure file is only included once
#ifndef SUM_H_INCLUDED // "C"-style header guard, 
#define SUM_H_INCLUDED // can be used alongside the new version

int sum(int i, int j); // declare function sum(...)

#endif /* SUM_H_INCLUDED */ // end "C"-style header guard 
