#include <iostream> // Include STL files
#include "sum.hpp"  // Include own header files. 
                    // We use " (double quotes) instead of < > (angle brackets)
                    // This files declares function sum(...)

int main()
{
   // We can call function sum as it is declared in "sum.hpp"
   std::cout << sum(2,3) << std::endl; 

   return 0;
}
