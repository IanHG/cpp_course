#include <iostream>

class X // Declare class of type X
{
  private: // private stuff. The internal implementation of the class
    int m; // declare member m

  public: // public/user interface
    X(int i=0): m(i) // constructor, will initialize data member m
    {
    }

    int mf(int n) // a member function
    {
      int old = m;
      m = n;
      return old;
    }
}; // remember to end class definition with semicolon

int main()
{
   X x; // Declare variable x of type X
   
   int old_m = x.mf(8);

   std::cout << old_m   << std::endl;
   std::cout << x.mf(2) << std::endl;
}
