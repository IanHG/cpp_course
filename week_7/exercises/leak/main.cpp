struct base
{
   base()
   {
   }

   ~base()
   {
   }
};

struct derived
   :  public base
{
   float*   member;

   derived()
      :  member(new float[8])
   {
   }

   ~derived()
   {
      delete[] member;
   }
};

int main()
{
   base* d = new derived;
   delete d;
}
