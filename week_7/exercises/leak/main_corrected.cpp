struct base
{
   base()
   {
   }

   virtual ~base()
   {
   }
};

struct derived
   :  public base
{
   float*   member;

   derived()
      :  member(new float[8])
   {
   }

   virtual ~derived()
   {
      delete[] member;
   }
};

int main()
{
   base* d = new derived;
   delete d;
}
