\documentclass[a4paper,12pt]{article}

\input{header.tex}
\input{codetypeset.tex}

\begin{document}
\normalsize
\begin{center}
\bf\large Exercises Week 7
\end{center}

\begin{enumerate}
%%%%%
%
%%%%%
\item{\bf Abstract and concrete}
   
   List some general concepts, and come up with some concrete examples of those concepts,
   e.g. Concept: Animal, Concrete Animals: Dog, Cat, Horse, etc.

%%%%%
%
%%%%%
\item{\bf CATS}
  
   Go to \iterminal{../examples/cats} (I suggest to copy the files somewhere else before modifying).
   
   Give \icpp{Felid} a \icpp{virtual} destructor.
  
   Implement for ALL cats another cat functionality, e.g. a \icpp{pet()} function.

   Implement more cat types, e.g. panther, puma, etc.!

   Add \icpp{ChessireCat} and all other cats you have implemented to the \icpp{FelidFactory} function,
   such that it can create ALL available cat types.

   Write a program that proves this is actually run-time polymorphism.
   \hiddenhint{Make a program that takes some user input and meows accordingly.
   Remember to handle bad input in a graceful way.}

   \emph{Extra:} In \iterminal{../examples/cats/main2.cpp} we use the \icpp{FelidFactory} to create ours cats,
   but just use \icpp{delete} to destroy them. 
   This a little asymetrical, as we have a naked call to \icpp{delete} without having 
   an explicit call to \icpp{new} in the code using our cat functionality.
   This may be considered bad practice.
   Make a function \icpp{FelidDestroy} that takes a \icpp{Felid*} and deallocates it.
   Use this function instead of \icpp{delete}.

   \emph{Extra extra (actually more important than Extra):} 
   We can do even better than having a \icpp{FelidDestroy} function, which needs to be called automatically.
   Using \icpp{std::unique_ptr} we can wrap the \icpp{Felid*}, and have it deallocated automatically 
   when it goes out of scope.
   Implement the \icpp{FelidFactory} such that it returns a \icpp{std::unique_ptr<Felid>} instead of a
   raw \icpp{Felid*}. 
   To get the raw pointer from a \icpp{std::unique_ptr} use \icpp{std::unique_ptr::get()}.
   Use this when passing the \icpp{Felid*} to \icpp{do_meowing_pointer}.
   Use \emph{Google-fu} to find information on \icpp{std::unique_ptr} if you have not 
   heard about it before.

%%%%%
%
%%%%%
\item{\bf People}

   Make a \icpp{Person} class that can be used to \icpp{pet} a \icpp{Felid}.
   The \icpp{Person} should have an internal health state, 
   that should be modified by the \icpp{pet} function, 
   e.g. petting a \icpp{Tiger} might not be good for your health.

   \emph{Extra:} Implement a hierarchy of people with different functionalities, 
   i.e. an animal trainer doesn't get hurt when petting a \icpp{Tiger}.
   
   \begin{center}
      \includegraphics{figures/people/people-figure0.pdf}
   \end{center}

%%%%%
%
%%%%%
\item{\bf Cage}

  Make a \icpp{Cage} class to hold any type of \icpp{Felid}.
  Through the cage you should be able to \icpp{pet} the cat,
  and when you do it should say \icpp{meow}.

  Make it such that when a \icpp{Person} pets a \icpp{Tiger} through a cage he doesn't get killed.

%%%%%
%
%%%%%
\item{\bf Clean-up}

   What is the problem with the code in \iterminal{leak/main.cpp}?

   If you cannot see the problem, try running the code through Valgrind.

   \begin{terminal}
      $ g++ -g main.cpp
      $ valgrind ./a.out
   \end{terminal}

   Fix the problem!

   \hiddenhint{Destructors should be \icpp{virtual}, else you will not call the destructor for \icpp{derived}, and you will leak the memory you have allocated in the constructor.}

%%%%%
%
%%%%%
\item{\bf Manual Vtable implementation}

   Dynamic polymophism in \cppname{} is usually implemented (behind the scenes) using something called \emph{vtables} or \emph{"virtual tables"}.
   A vtable is basically a table of function pointers managed by the base-class, which is then set to point to the functions of the specific derived-class.
   In this exercise we will try to implement a vtable by hand, and not use the \icpp{virtual}-keyword to implement dynamic polymorphism.

   {\bf Stage 0 : Boiler plate}

      First take a look at the boiler plate code in \iterminal{vtable_impl/stage0/main.cpp}.
      Here we have a basic \icpp{vtable}, holding a single function pointer, to a function named \icpp{func}.

      \includecpplines[Vtable]{vtable_impl/stage0/main.cpp}{3}{6}

      We have \icpp{base}-class, which holds a pointer to a \iterminal{vtable}, 
      and when we call \icpp{func} on the \icpp{base}-class,
      we delegate the call to the function pointed to by the \icpp{vtable}.

      \includecpplines[Vtable]{vtable_impl/stage0/main.cpp}{16}{19}

      The special \icpp{->*} operator means "pointer to member function", 
      as member functions are a special kind of functions (see e.g. \href{http://www.parashift.com/c++-faq-lite/pointers-to-members.html}{this FAQ} for more information).
      For now you can also just accept that this it how the syntax looks.

   {\bf Stage 1 : Implement \icpp{derived}}

      We will now try to implement a \icpp{derived}-class.
      Make a new class called \icpp{derived} and give it a \icpp{func}-function,
      which prints a unique message. The \icpp{derived}-class should inherit publicly from \icpp{base}.

      Give \icpp{derived} a \icpp{static} member variable of type \icpp{vtable}.

      \includecpplines[Vtable]{vtable_impl/stage1/main.cpp}{43}{43}

      And below the \icpp{derived}-class we initialize this object with a pointer to the \icpp{func}-function of \icpp{derived} that we just implemented.

      \includecpplines[Vtable]{vtable_impl/stage1/main.cpp}{46}{46}

      Pass a pointer to the \icpp{static vtable} to \icpp{base}, when constructing \icpp{derived}.

      Try to allocate a pointer to \icpp{derived}, but save it as a pointer to \icpp{base}.
      Call \icpp{func} on the pointer.
      Remember to de-allocate the pointer again afterwards.

      If you get stuck, look at the code in \iterminal{vtable_impl/stage1/main.cpp}.

   {\bf Stage 2 : Destructor}

      Can you see a problem with the code we have just implemented? 
      What happens when we de-allocate the pointer to \icpp{base}? 
      Is the destructor of \icpp{derived} getting called?

      We need to add a destructor to the \icpp{vtable}. 
      The signature is \icpp{void (base::*)()} and give a name like \icpp{destroy}.
      Implement a destructor for \icpp{base}, and make it call the \icpp{destroy} function from the \icpp{vtable}.
      Implement a \icpp{destroy} function for \icpp{derived} and add it to the initialization of the \icpp{vtable} for \icpp{derived}.

      If you get stuck, look at the code in \iterminal{vtable_impl/stage2/main.cpp}.

   {\bf Stage 3 : Default virtual functions }

      Think about how to implement a default function in \icpp{base}, which should be called if the \icpp{derived} class does not implement a specific function.

      In \iterminal{vtable_impl/stage3/main.cpp} is an implementation that shows how one can add default functions from \icpp{base} to \icpp{derived} class vtables.

   {\bf Stage 4 : \icpp{base} objects }

      To allow a naked \icpp{base} object to be constructed (not nescessarily a good feature), 
      we need to add a static virtual table object for the \icpp{base} class as well,
      and initialize it with the default functions from \icpp{base}.

      One thing to note is that if we create a \icpp{destroy} function for \icpp{base},
      we need to make sure that we call this, when we \icpp{destroy} a \icpp{derived} object.

      Implemented the possibility to construct a \icpp{base} object.

      In \iterminal{vtable_impl/stage4/main.cpp} is shown an implementation, which allows objects of type \icpp{base},
      and handles the destruction of \icpp{base} for \icpp{derived} types by calling \icpp{base::destroy()} 
      from \icpp{derived::destroy}. There are also other possibilities for how this could be implemented.

   {\bf Final}

      You have now manually implemented a virtual table, which can be used for dynamic polymorphism. 
      Our implementation is somewhat close to how the compiler implements virtual tables, 
      theres is one place where it fall a little short. What could the difference be (think about it, and then read the hint)?

      \hiddenhint{We do not have the possibility to make a \emph{pure abstract} \icpp{base} class, 
      i.e. we do not have possibility to mark a \icpp{base} class as pure, 
      and have the safety of the compiler checking whether we ever construct a naked \icpp{base} object.}

      The code in \iterminal{vtable_impl/final} is the final code, and is the same as the last stage.
      Alreade at \iterminal{stage2} we have pretty good implementation, 
      and \iterminal{stage3} and \iterminal{stage4} are just some extra bells and whistles.

%%%%%
%
%%%%%
\item{\bf Yin?}

  Do exercises 8.7.8, 8.7.9, and 8.7.10 in Yang.

%%%%%
%
%%%%%
\item{\bf No \iterminal{ProjectEuler} today? }

   Of course! Go get at them problems, 
   and if you can solve a problem using dynamic-polymorphism do it!

   \iterminal{https://projecteuler.net/}

\end{enumerate} 

\end{document}
