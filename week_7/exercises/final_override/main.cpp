#include <iostream>

class Base
{
   public:
      virtual void do_stuff() override
      {
         std::cout << " Doing Base stuff " << std::endl;
      }
};

class Derived: public Base
{
   public:
      virtual void do_stuff() final
      {
         std::cout << " Doing Derived stuff " << std::endl;
      }
};

class DerivedDerived: public Derived
{
   public:
      void do_stuff() final
      {
         std::cout << " Doing DerivedDerived stuff " << std::endl;
      }
};


class DerivedA: public Base
{
   public:
      virtual void do_other_stuff() override
      {
         std::cout << " Do other DerivedA stuff " << std::endl;
      }
};


void stuff(Base& base)
{
   base.do_stuff();
}

int main()
{
   Base base;
   Derived derived;
   DerivedDerived derivedderived;

   DerivedA deriveda;

   stuff(base);
   stuff(derived);
   stuff(derivedderived);
   stuff(deriveda);

   return 0;
}
