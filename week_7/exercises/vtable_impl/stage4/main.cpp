#include <iostream>
#include <memory>
#include <vector>

class base;

struct vtable
{  
   void (base::* const destroy)();
   void (base::* const func)();
};

class base
{
   public:
      base(vtable* vtbl_)
         :  vtbl(vtbl_)
      {
      }

      base()
         :  base(&vtbl_)
      {
      }

      ~base()
      {
         (this->*(vtbl->destroy))();
      }

      void destroy()
      {
         std::cout << " destroy base " << std::endl;
      }

      void func_default()
      {
         std::cout << " base " << std::endl;
      }

      void func()
      {
         (this->*(vtbl->func))();
      }

   private:
      const vtable* const vtbl;

      static vtable vtbl_;
};

vtable base::vtbl_ = 
   {  static_cast<void (base::* const)() >(&base::destroy) 
   ,  static_cast<void (base::* const)() >(&base::func_default) 
   };

class derived
   : public base
{
   public:
      derived()
         :  base(&vtbl_)
      {
      }

      ~derived()
      {
         this->destroy();
      }

      void destroy()
      {
         std::cout << " destroying derived " << std::endl;
         base::destroy();
      }

      void func()
      {
         std::cout << " derived " << std::endl;
      }

   private:
      static vtable vtbl_;
};

vtable derived::vtbl_ = 
   {  static_cast<void (base::* const)() >(&derived::destroy) 
   ,  static_cast<void (base::* const)() >(&derived::func) 
   };

class derived2
   : public base
{
   public:
      derived2()
         :  base(&vtbl_)
      {
      }

      ~derived2()
      {
         this->destroy();
      }

      void destroy()
      {
         std::cout << " destroying derived2 " << std::endl;
         base::destroy();
      }

   private:
      static vtable vtbl_;
};

vtable derived2::vtbl_ = 
   {  static_cast<void (base::* const)() >(&derived2::destroy) 
   ,  static_cast<void (base::* const)() >(&base::func_default) 
   };

int main()
{
   std::vector<std::unique_ptr<base> > vec;

   vec.emplace_back(std::unique_ptr<base>{ new base     });
   vec.emplace_back(std::unique_ptr<base>{ new derived  });
   vec.emplace_back(std::unique_ptr<base>{ new derived2 });

   for(const auto& elem : vec)
   {
      elem->func();
   }
}
