class base;

struct vtable
{  
   void (base::* const func)();
};

class base
{
   public:
      base(vtable* v)
         :  vtbl(v)
      {
      }

      void func()
      {
         (this->*(vtbl->func))();
      }

   private:
      const vtable* const vtbl;
};

int main()
{
}
