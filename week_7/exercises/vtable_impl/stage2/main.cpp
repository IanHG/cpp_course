#include <iostream>
#include <memory>

class base;

struct vtable
{  
   void (base::* const destroy)();
   void (base::* const func)();
};

class base
{
   public:
      base(vtable* vtbl_)
         :  vtbl(vtbl_)
      {
      }

      ~base()
      {
         (this->*(vtbl->destroy))();
      }

      void func()
      {
         (this->*(vtbl->func))();
      }

   private:
      const vtable* const vtbl;
};

class derived
   : public base
{
   public:
      derived()
         :  base(&vtbl_)
      {
      }

      ~derived()
      {
         this->destroy();
      }

      void destroy()
      {
         std::cout << " destroying derived " << std::endl;
      }

      void func()
      {
         std::cout << " derived " << std::endl;
      }

   private:
      static vtable vtbl_;
};

vtable derived::vtbl_ = 
   {  static_cast<void (base::* const)() >(&derived::destroy) 
   ,  static_cast<void (base::* const)() >(&derived::func) 
   };

int main()
{
   std::unique_ptr<base> b { new derived };

   b->func();
}
