#include <iostream>
#include <memory>

class base;

struct vtable
{  
   void (base::* const func)();
};

class base
{
   public:
      base(vtable* v)
         :  vtbl(v)
      {
      }

      void func()
      {
         (this->*(vtbl->func))();
      }

   private:
      const vtable* const vtbl;
};

class derived
   : public base
{
   public:
      derived()
         :  base(&vtbl_)
      {
      }

      void func()
      {
         std::cout << " derived " << std::endl;
      }

   private:
      static vtable vtbl_;
};

vtable derived::vtbl_ = { static_cast<void (base::* const)() >(&derived::func) };

int main()
{
   std::unique_ptr<base> b { new derived };

   b->func();
}
