#ifndef BASE_H_INCLUDED
#define BASE_H_INCLUDED

#include <iostream>

struct Base {
   public: 
      virtual void do_stuff() {
         std::cout << " Doing some Base class stuff " << std::endl;
      }
};

class DerivedA: public Base {
   public: 
      virtual void do_stuff() {
         std::cout << " Doing some DerivedA class stuff " << std::endl;
      }
};

class DerivedB: public Base {
   public: 
      virtual void do_stuff() {
         std::cout << " Doing some DerivedB class stuff " << std::endl;
      }
};

#endif /* BASE_H_INCLUDED */
