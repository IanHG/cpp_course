#include "base.h"

void stuff_reference(Base& b)
{
   b.do_stuff();
}

void stuff_pointer(Base* b)
{
   b->do_stuff();
}

int main()
{
   Base base;
   DerivedA deriveda;
   DerivedB derivedb;

   stuff_reference(base);
   stuff_pointer(&deriveda);

   return 0;
}
