#include <iostream>

class Base
{
   public:
      virtual ~Base()
      {
      }
};

class Derived: public Base
{
   int* i = new int;
   public:
      ~Derived()
      {
         delete i;
      }
};

int main()
{
   Base* base = new Derived;
   delete base;
   return 0;
}
