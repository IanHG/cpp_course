#ifndef CAT_FACTORY_H_INCLUDED
#define CAT_FACTORY_H_INCLUDED

#include <cstdlib> // for exit()
#include "cats.h"

enum FELIDS { CAT, TIGER, OCELOT }; // define numeric constants

Felid* FelidFactory(FELIDS felids) {
   switch(felids) {
      case CAT: // if input is CAT
         return new Cat(); // create and return Cat object
      case TIGER:
         return new Tiger();
      case OCELOT:
         return new Ocelot();
      default: // default case
         std::cout << "INPUT NOT VALID " << std::endl;
         exit(42);
   }
   return nullptr; // will never get here, but will silence compiler warning
}

#endif /* CAT_FACTORY_H_INCLUDED */
