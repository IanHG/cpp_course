#include "catfactory.h"

void do_meowing_pointer(const Felid* const cat)
{
   cat->meow();
}

int main()
{
   Felid* felid = FelidFactory(CAT); // create a cat with Factory function
   do_meowing_pointer(felid); 
   delete felid; // remember to deallocate pointer

   return 0;
}
