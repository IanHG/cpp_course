#include "cats.h"

void do_meowing(const Felid& cat)
{
   cat.meow();
}

int main()
{
   Cat cat;
   Tiger tiger;
   Ocelot ocelot;

   do_meowing(cat);
   do_meowing(tiger);
   do_meowing(ocelot);

   // I can run with a possible new type without changing the code of 
   // the do_meowing() function
   ChessireCat chessir;
   do_meowing(chessir);
}
