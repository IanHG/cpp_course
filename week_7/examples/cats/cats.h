#include <iostream>

class Felid { // abstract base class
   public:
       virtual void meow() const = 0; // pure virtual function
};

class Cat : public Felid { // derived class... inherit from base
   public:
       void meow() const { std::cout << "Meowing like a regular cat! meow!\n"; }
};

class Tiger : public Felid { // derived class... inherit from base
   public:
       void meow() const { std::cout << "Meowing like a tiger! MREOWWW!\n"; }
};

class Ocelot : public Felid { // derived class... inherit from base
   public:
       void meow() const { std::cout << "Meowing like an ocelot! mews!\n"; }
};

// We can extend with a new class later that was not part of the original hierarchy.
class CheshireCat: public Felid {
  public:
    void meow() const { std::cout << "Grinning like a Cheshire Cat. Hi-hi-hi \n"; }
};
