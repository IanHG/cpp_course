\documentclass[t]{beamer}
\usetheme{CambridgeUS}

\input{header.tex}
\input{codetypeset.tex}
\input{ls.tex}

\begin{document}
\title{Dynamic Polymorphism in \cppname{}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   \titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Today
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   {Today!}

   Today we will cover:
   \begin{itemize}
      \item
        Polymorphism
      \item
        \icpp{virtual} functions
      \item
        Factory methods
      \item
        Abstract classes
   \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Polymorphism}

   {\bf Polymorphism:} The condition of occurring in several different forms.
   
   \begin{description}
      \item[Genetics]
      The presence of genetic variation within a population, 
      upon which natural selection can operate.
      \begin{center}
         \includegraphics[width=0.6\textwidth]{../figures/butterfly_polymorphism.png}
      \end{center}
      \item[Computing]
      A feature of a programming language that allows routines 
      to use variables of different types at different times
   \end{description}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Virtual functions}

   Virtual functions in a base class can be overridden within a derived class by a function with the same signature. \\
   \verticalspace
   Virtual functions are declared by prepending function declarations with keyword \icpp{virtual}. \\
   \verticalspace
   Virtual functions allow {\bf polymorphic} behaviour through pointer-to-base class or reference-to-base class. \\
   \verticalspace
   Allows for code-reuse, easy code modification and easy code extension!
   
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {What we want to achieve}

   \begin{center}
     \includegraphics[width=0.8\textwidth]{../figures/virtual_cut.png}
   \end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Cats! as an abstract concept}

   \begin{center}
   \includegraphics[width=\textwidth]{figures/cats/cats-figure0}
   \end{center}
   \verticalspace
   {\bf Felid:} General concept. \\
   \verticalspace
   {\bf Cat, Tiger, and Ocelot:} Are specific implementations of the concept.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Implementing a cat zoo}
   
   \includecpplines[Cat implementtion]{examples/cats/cats.h}{3}{21}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Usage}

   Call functions through reference to object of base type
   
   \includecpplines[main]{examples/cats/main.cpp}{3}{6}
   
   \includecpplines[main]{examples/cats/main.cpp}{10}{16}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Running the code}
   
   \begin{terminal}
      $ g++ main.cpp
      $ ./a.out
      Meowing like a regular cat! meow!
      Meowing like a tiger! MREOWWW!
      Meowing like an ocelot! mews!
   \end{terminal}
   \verticalspace
   \verticalspace
   Can also call through pointer to object of base type

   \includecpplines[Pointer-to-base]{examples/cats/main2.cpp}{3}{6}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Checklist}

   {\bf Code reuse:} \icpp{do_meowing()} is only implemented \emph{once}, and works for ALL \icpp{Felid}s \\
   \verticalspace
   {\bf Easy extension:} I can easily implemenent another cat-creature and all functions working for \icpp{Felid}
   will automatically work for my new cat!
   
   \includecpplines[Follow the white rabbit]{examples/cats/cats.h}{24}{27}

   \includecpplines[This now works]{examples/cats/main.cpp}{18}{21}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Factory methods}
   
   A \emph{factory} is a function that creates an instance 

   \includecpplines[Cat factory]{examples/cats/catfactory.h}{7}{22}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Factory usage}

   We can now create pointers to \icpp{Felid}, which points to
   specific derived implementations.

   \verticalspace
   
   \includecpplines[Factory usage]{examples/cats/main2.cpp}{10}{12}

   \verticalspace
   
   \begin{terminal}
      $ g++ main2.cpp
      $ ./a.out
      Meowing like a regular cat! meow!
   \end{terminal}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{A slight problem}
We can create an object of type Felid.
\begin{cpp}
class Felid { // base class
  public: // virtual function in base class
    virtual void meow() const { throw runtime_error("Felid::meow"); }
};

// ... some code ...

Felid felid;
felid.meow(); // will throw an error at run-time
\end{cpp}
Using object of type Felid will fail at run-time. \\
\verticalspace
Makes no sense to have object of type Felid! \\
\verticalspace 
{\bf Solution: } Pure \icpp{virtual} functions and \emph{abstract classes}!
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Abstract Base classes}
A virtual function is made pure by using the 'initializer' \icpp{= 0}
\begin{cpp}[Abstract Felines]
class Felid { // abstract class
  public:
    virtual void meow() const = 0; // pure virtual function
};
\end{cpp}
Any class with any pure virtual function is an \emph{abstract class}. \\
\verticalspace
No objects of an abstract class can be created.
\begin{cpp}[Error]
Felid felid; // Felid is abstract => COMPILATION ERROR!
\end{cpp}
We now catch the error at {\bf compile-time}!
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{frame}[fragile]{\icpp{final} and \icpp{override}}
%  hey
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Caveat}
\begin{cpp}[Leaking Memory]
class Base
{
};
                     
class Derived: public Base
{
  int* i = new int; // allocated memory
  public:
    ~Derived() // destructor
    {
      delete i; // in destructor we de-allocate
    }
};

int main()
{
  Base* base = new Derived; // allocate pointer to Derived
  delete base; // delete through pointer to Base => memory leak!
  return 0;
}
\end{cpp}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Virtual Destructors}
  \begin{cpp}[Virtual destructor]
class Base
{
  public:
    virtual ~Base() { }
};

int main()
{
  Base* base = new Derived; // allocate pointer to Derived
  delete base; // delete through pointer to Base => virtual d-tor
               // will call ~Derived()! => NO MEMORY LEAK!
  return 0;
}
  \end{cpp}
  \verticalspace
  {\bf Best practice:} If a class has any \icpp{virtual} function it should have a 
  \icpp{virtual} destructor!
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Summary}

   {\bf Pros:} \\
   \begin{itemize}
      \item Easy code maintenance
      \item Easy code extensibility
      \item Minimum code duplication
   \end{itemize}
   \verticalspace
   {\bf Cons:} \\
   \begin{itemize}
      \item Slight overhead in calling \icpp{virtual} functions (table look-up)
      \item Don't use for functions that are called millions of times, when going for high-performance
      \item Designing good abstractions can be hard!
   \end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\begin{center}
\Large
DONE! Now lets code.
\end{center}
\end{frame}

\end{document}
