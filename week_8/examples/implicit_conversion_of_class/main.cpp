class A {};

class B 
{
   public:
      B (const A& x) {} //conversion from A (constructor):
      B& operator= (const A& x) {return *this;} // conversion from A (assignment):
      operator A() {return A();}// conversion to A (type-cast operator)
};
        
int main ()
{
   A foo;
   B bar = foo;    // implicit conversion through constructor
   bar = foo;      // implicit conversion through assignment
   foo = bar;      // implicit conversion through type-cast operator
   return 0;
}
