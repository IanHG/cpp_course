#include <iostream>

void print (char * str) { // someone made a bad interface to print
   std::cout << str << '\n';
}

int main () {
   const char * c = "sample text";
   print ( const_cast<char *> (c) ); // print takes char*, we use const_cast
   return 0;                         // to also be able to print const char*
}
