class A {};

class B 
{
   public:
      explicit B (const A& x) {}
};

void func (B x) {}

int main ()
{
   A foo;
   B bar (foo); // explicit conversion through constructor

   func (foo);  // not allowed for explicit ctor => COMPILATION ERROR
   func (bar);  // ok: bar is of type B
}
