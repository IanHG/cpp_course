#include <iostream>

class Base { virtual void dummy() {} };
class Derived: public Base { int a; };

int main () 
{
   Base * pba = new Derived;
   Base * pbb = new Base;
   Derived * pd;
   
   pd = dynamic_cast<Derived*>(pba); // ok: pba actually points to Derived*
   if (!pd) std::cout << "nullptr on first type-cast.\n";

   pd = dynamic_cast<Derived*>(pbb); // pbd does not point to Derived*
   if (!pd) std::cout << "nullptr on second type-cast.\n";

   return 0;
}
