#include <iostream>

class Dummy {
   double i=2.0,j=3.0;
};

class Subtraction {
   int x,y;
   public:
      int result() { return x-y;}
};

int main () {
   Dummy d;
   Subtraction* padd = (Subtraction*) &d; // explicitly type cast Dummy* to Subtraction*
   std::cout << padd->result() << std::endl; // use Subtraction::result() on Dummy*
   return 0;
}
