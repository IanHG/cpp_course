class A {};

class B {
   public:
      explicit B(const A&) {};
};


int main()
{
   A a;
   B b = a;
}
