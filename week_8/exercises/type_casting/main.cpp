#include <iostream>

int main()
{
   unsigned ui = 10;
   int ii{ui};
   
   for(int i=0; i<ui; ++i)
   {
      std::cout << i+ii << std::endl;
   }

   return 0;
}
