C++ course
=========================================================

Divided into material for each week, each located in a `week_<N>` folder.
Each week consist of lecture slides, with some examples and exercises.
One does not have to follow the weeks in order, but some of the later lectures
builds on the the earlier lectures.

To build lecture slides, change directory to one of the `week_<N>` folders and type `make`.
E.g. to build slides for `week_1` do:

```sh
cd week_1
make
```

This will generate a `lecture_<N>.pdf` with the slides.

Same goes for exercises:

```sh
cd week_1/exercises
make
```

This will create an `exercises.pdf` with the weeks exercises. 
To generate slides and and exercises you will need `pdflatex`.

Slides and exercises come with small code examples,
that can be inspected, compiled and executed.
Some are located in the `examples` folder 
and some are located in the `exercises` folder.
Most snippets presented in the slides should be included in compilable source files.
If this is the case the path to the file in written in the upper right corner of the code block.

Overview
===========================================================

Week 1
-----------------------------------------------------------
   - Hello world
   - POD (Plain-Old-Data) types
   - Control structures
      -- for, while, if, etc
   - STL vector (std::vector)

Week 2
-----------------------------------------------------------
   - const keyword
   - Pointers and references
   - Functions
      -- Pass by value, pass by reference
      -- Default arguments
   
Week 3
-----------------------------------------------------------
   - Multiple source files
   - Basic C++ classes

Week 4
-----------------------------------------------------------
   - Operator overloading
   - More class stuff
      -- Default functions
      -- Self addressing with 'this' pointer

Week 5
-----------------------------------------------------------
   - Forward declaration
   - Templates
      -- Function templates
      -- Class templates
      -- Organizing template code

Week 6
-----------------------------------------------------------
   - 'public' inheritance

Week 7
-----------------------------------------------------------
   - Dynamic polymorphism (cat example)
   - Pure virtual functions and abstract base classes

Week 8
-----------------------------------------------------------
   - Type conversion
      -- Implicit and explicit
   - Type casting
      -- static_cast, dynamic_cast, reinterpret_cast, const_cast

Week 9
-----------------------------------------------------------
   - l-values and r-values
   - Move semantics and r-value references
   - Return Value Optimization (RVO)

Week 10
-----------------------------------------------------------
   - STL containers and algorithms
      -- std::vector
      -- std::accumulate and std::sort

Week 11
-----------------------------------------------------------
   - Functors
   - Lambdas
   - std::bind

