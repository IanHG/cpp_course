\documentclass[t]{beamer}
%\usetheme{Darmstadt}
\usetheme{CambridgeUS}

\input{header.tex}
\input{codetypeset.tex}
\input{ls.tex}

\begin{document}
\title{Pointers and Functions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Outline
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Today}
   Today we will talk about:
   
   \begin{itemize}
      \item
      The \icpp{const} keyword
      \item
      Pointers and references
      \item
      Functions in \cppname{}
   \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'const' keyword
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\icpp{const} keyword}
   
   \icpp{const} means roughly "I promise not to change this value"

   \includecpplines[]{examples/const/main.cpp}{8}{13}
   
   \verticalspace
   \icpp{const} provides compile-time safety that program constants do not
   change during its lifetime (\emph{e.g.} in a function or the whole program). \\
   \verticalspace
   \icpp{const}-correctness, \emph{i.e.} making sure constants are marked \icpp{const}, 
   is very important for correctness and maintainability of computer code.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Pointers}

   \begin{columns}
      \begin{column}{0.48\textwidth}
         Each byte of computer memory is addressable using hexadecimal addresses.

         \verticalspace

         Pointers are a special type capable of holding such a memory address.

         \verticalspace
         
         A pointer points to the address of the first byte of the variable.

         \verticalspace
         
         \begin{figure}
           \includegraphics[scale=0.8]{figures/memory/memory2-figure0.pdf}
         \end{figure}


      \end{column}
      \begin{column}{0.48\textwidth}
         \begin{figure}
           \includegraphics[scale=0.8]{figures/memory/memory-figure0.pdf}
         \end{figure}
      \end{column}
   \end{columns}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Pointers}

   \includecpplines[]{examples/pointer_basic/pointer_basic.cpp}{5}{11}

   In declaration \icpp{*} means "pointer to", and in expressions \icpp{*} means "contents of"  \\

   \begin{terminal}
      $ ./pointer_basic.x
      i   = 3
      pi  = 0x7ffdfcc56f2c
      *pi = 3
   \end{terminal}
   
   Adresses are given in hexadecimal numbers.

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Example}
   \includecpplines[Example: Pointer]{examples/pointer_quiz/pointer_quiz.cpp}{5}{16}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Pointers and Arrays}

   \includecpplines[Arrays and pointers]{examples/array/array.cpp}{3}{9}
   
   In declarations \icpp{[ ]} means "array of" \\
   \verticalspace
   In expressions \icpp{[ ]} means "element of" \\
   
   \begin{center}
     \begin{figure}
       \includegraphics[scale=0.8]{figures/pointer/pointer-figure0.pdf}
     \end{figure}
   \end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pointer examples
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\icpp{nullptr}}
   \begin{center}
      \begin{figure}
         \includegraphics[scale=0.22]{../figures/nullptr.jpg}
      \end{figure}
   \end{center}
   \includecpplines[NULL]{examples/nullptr/nullptr.cpp}{5}{8}
   Dereferencing a \icpp{nullptr} is \emph{undefined behaviour}!
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Null ptr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Boolean evaluation of pointers}
   \includecpplines[Example: Boolean evaluation]{examples/pointer_bool/pointer_bool.cpp}{5}{10}
   \includecpplines[Example: NULL]{examples/pointer_bool/pointer_null.cpp}{5}{10}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {References}
   A reference is an alternative name or alias for an object/variable. \\
   \verticalspace
   Almost like a pointer, but with object syntax \\
   \verticalspace
   References are declared by appending type with \icpp{&} \\
   \verticalspace
   Unlike pointers, references must ALWAYS be initialized
   $\rightarrow$ safety
   
   \includecpplines[Example: References]{examples/references/references.cpp}{6}{14}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Operator matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Welcome to the Matrix}
   
      \begin{center}
         \begin{tabular}{|c|cc|}
            \hline
            Operator   & Declaration  & Expression \\ \hline
            \icpp{*}   & pointer-to   & value-of   \\
            \icpp{&}   & reference-to & address-of \\
            \icpp{[ ]} & array        & element-of \\
            \hline
         \end{tabular}
      \end{center}

      \includecpplines[]{examples/operator_matrix/operator_matrix.cpp}{5}{12}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pop quiz
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Pop quiz}
   \includecpp[What will be printed?]{examples/pop_quiz/pop_quiz.cpp}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Answer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Answer}
  \begin{terminal}
$ ./pop_quiz.x
i   = 2
&i  = 0x7fff508948fc
pi  = 0x7fff508948fc
*pi = 2
&pi = 0x7fff50894900
ri  = 2
&ri = 0x7fff508948fc
  \end{terminal}
\verticalspace
   Memory adresses are printed as hexadecimal numbers (you will see different addresses)
\verticalspace
\begin{center}
\begin{tabular}{lcccc}
   Object  & \icpp{i}               & \icpp{pi}              & \icpp{ri}              \\ \hline
   Adresse & \icpp{0x7fff508948fc}  & \icpp{0x7fff50894900}  & \icpp{0x7fff508948fc}  \\
   Value   & \icpp{2}               & \icpp{0x7fff508948fc}  & \icpp{2} 
\end{tabular}
\end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pointers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Dynamic allocation: operators \icpp{new} and \icpp{delete}}
   A pointer is the address in memory of an object
   \includecpp[Allocating a pointer]{examples/pointer/pointer.cpp}

   Example values:
   \begin{center}
      \scriptsize
      \begin{tabular}{lcccccc}
      expr:    && \icpp{p}                               && \icpp{p[0]} &
      \icpp{p[1]} & \icpp{p[2]} \\ \cline{3-3} \cline{5-7}
      value:   && \multicolumn{1}{|c|}{\icpp{0x1bae010}} &&
      \multicolumn{1}{|c}{\icpp{1.0}} & \multicolumn{1}{|c}{\icpp{2.0}} &
      \multicolumn{1}{|c|}{\icpp{3.0}} \\ \cline{3-3} \cline{5-7}
      address: && \icpp{0x7fffba395040}                  &&  \icpp{0x1bae010}
      & \icpp{0x1bae018} & \icpp{0x1bae020}
      \end{tabular}
   \end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Example: Operators \icpp{new} and \icpp{delete}}
   \includecpp{examples/new/main.cpp}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Functions}
   \begin{cpp}[Function declaration and defintion]
£\emph{return type}£ £ £ func(£\emph{...function arguments...}£)
{
   £\emph{function body}£
}
   \end{cpp}
   Declaration and definition of a function taking a set of \emph{function
   arguments} executing the \emph{function body} and returning a variable of type
   \emph{return type}. \\
   \verticalspace
   Having \icpp{void} \emph{return type}, means nothing is returned.

   \includecpplines[Square $f(x) = x^2$]{examples/square_function/program1.cpp}{3}{7}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions example
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Using functions}
  \includecpp[Function call]{examples/square_function/program1.cpp}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Passing multiple arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Pass multiple arguments}
   \includecpplines[Implementation]{examples/multiarg/multiarg.cpp}{3}{6}
   \includecpplines[Usage]{examples/multiarg/multiarg.cpp}{10}{12}

   \verticalspace
   Even though we can pass multiple arguments, we can only return one value
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function overloading
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Function overloading}
   \includecpplines[Overloading]{examples/overloading/overloading.cpp}{3}{13}
   \includecpplines[Overloading usage]{examples/overloading/overloading.cpp}{17}{21}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pass by value or reference
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Pass value or reference}
   Functions in \cppname{} are either \emph{pass by value} or \emph{pass
   by reference}

   \includecpplines[Argument passing]{examples/functions/main.cpp}{3}{14}

NB FORTRAN USERS: In FORTRAN all functions are \emph{pass by pointer
(reference)} 
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Using \icpp{square(...)}}
   \includecpplines[Argument passing usage]{examples/functions/main.cpp}{16}{27}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pass by reference
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\icpp{const} \emph{ref}}
   \includecpplines[Passing const reference]{examples/const_ref/const_ref.cpp}{3}{7}
   
   \verticalspace
   I pass a reference to \icpp{i} so no copy is made \\
   \verticalspace
   I promise not to change \icpp{i} in the function \\
   \verticalspace
   Used extensively for higher-level structures that are expensive to copy!
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Default function arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Default function arguments}
   \includecpplines[Default argument]{examples/default_arg/default_arg.cpp}{3}{6}
   \verticalspace
   Defaulted arguments must always be at the end of the argument list
   \verticalspace
   \includecpplines[Default argument]{examples/default_arg/default_arg.cpp}{10}{13}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OK DONE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   \begin{center}
      OK enough for today, lets do some coding!
   \end{center}
\end{frame}


\end{document}
