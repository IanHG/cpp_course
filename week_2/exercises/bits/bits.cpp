#include <bitset>
#include <iostream>

int main()
{
   int i = 1378249; // Declare integer i with value 1378249

   int*  pi = &i;   // Get the address of i
   char* pc = (char*)pi; // Interpret as individual bytes
   
   // Loop over each byte and print the bits
   for(int j = 0; j < sizeof(int); ++j)
   {
      std::bitset<8> byte(pc[j]);     // Convert byte to binary (bits)
      std::cout << byte << std::endl; // Print bits of each byte
   }
}
