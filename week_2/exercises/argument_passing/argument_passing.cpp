#include <iostream>

void pass_by_value(int i)
{
   i = 5;
}

void pass_by_reference(int& i)
{
   i = 6;
}

void pass_by_pointer(int* pi)
{
   *pi = 7;
}

int main()
{
   int i = 2;
   int j = 8;
   int k = 19;
   
   pass_by_value(i);
   pass_by_reference(j);
   pass_by_pointer(&k);

   std::cout << i << std::endl;
   std::cout << j << std::endl;
   std::cout << k << std::endl;

   return 0;
}
