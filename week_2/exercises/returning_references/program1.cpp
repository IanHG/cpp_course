#include<iostream>

int& squareinplace(int& i)
{
   i *= i; // multiply i by itself and store in i
   return i; // return reference to i
}

int main()
{
   int  i      = 3;
   int& iref   = squareinplace(i); // iref is now a reference to i

   std::cout << i << std::endl;
   std::cout << iref << std::endl;

   iref = 42;
   
   std::cout << i << std::endl;
   std::cout << iref << std::endl;

   return 0;
}
