#include<iostream>

int& sum(int i, int j)
{
   int sum = i + j;
   return sum;
}

int main()
{
   int& s1 = sum(2,4);
   int& s2 = sum(3,4);

   std::cout << s1 << std::endl;
   std::cout << s2 << std::endl;

   return 0;
}
