#include <iostream>

void pass_by_pointer(int* pi)
{
   std::cout << "Address of pi  inside function: " << &pi  << std::endl;
   std::cout << "Address of i   inside function: " << pi  << std::endl;
   std::cout << "Value   of i   inside function: " << *pi << std::endl;
}

int main()
{  
   int* pi = new int(3);
   
   std::cout << "Address of pi outside function: " << &pi  << std::endl;
   std::cout << "Address of i  outside function: " << pi  << std::endl;
   std::cout << "Value   of i  outside function: " << *pi << std::endl;

   pass_by_pointer(pi);

   delete pi;

   return 0;
}
