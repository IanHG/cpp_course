#include <iostream>

double* allocate(int size)
{
   double* ptr;

   if(size <= 10)
   {
      ptr = new double[size];
   }
   else
   {
      ptr = nullptr;
   }

   return ptr;
}

void deallocate(double* ptr)
{
   delete[] ptr;
}

int main()
{
   int size;

   std::cin >> size;

   double* ptr = allocate(size);

   for(int i = 0; i < size; ++i)
   {
      ptr[i] = double(i + 1.0);
   }

   for(int i = 0; i < size; ++i)
   {
      std::cout << ptr[i] << std::endl;
   }

   deallocate(ptr);

   return 0;
}
