#include <iostream>

int main()
{
   double* pd = new double; // allocate space for a single double
   delete pd; // deallocate

   double* pd1 = new double(5); // allocate space for a single double 
                                // and initialize value to 5
   delete pd1; // deallocate

   double* pd2 = new double[5]; // allocate 5 doubles 
   pd2[3] = 2.0; // access 4th element (like with arrays)
   delete[] pd2; // deallocate, now using delete[] as we allocated an array

   return 0;
}
