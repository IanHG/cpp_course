#include <iostream>

int main()
{
   int i = 2;
   int* pi = &i;
   int & ri = i;

   std::cout << "i   = " << i << std::endl;
   std::cout << "&i  = " << &i << std::endl;
   std::cout << "pi  = " << pi << std::endl;
   std::cout << "*pi = " << *pi << std::endl;
   std::cout << "&pi = " << &pi << std::endl;
   std::cout << "ri  = " << ri << std::endl;
   std::cout << "&ri = " << &ri << std::endl;
   
   return 0;
}
