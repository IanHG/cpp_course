#include<iostream>

int main()
{
   double* p = new double[3];   // allocate pointer
   
   std::cout << p  << std::endl;
   std::cout << &p << std::endl;

   for(int i = 0; i < 3 ; ++i)
   {
      p[i] = i + 1;  // assign elements
      std::cout << p[i]  << std::endl;
      std::cout << &p[i] << std::endl;
   }

   delete[] p; // deallocate pointer

   return 0;
}
