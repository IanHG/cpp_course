int main()
{
  double* p = new double[3]; // allocate pointer
  for(int i = 0; i < 3; ++i)
  {
    p[i] = i + 1; // assign elements
  }
  delete[] p; // deallocate pointer
  return 0;
}
