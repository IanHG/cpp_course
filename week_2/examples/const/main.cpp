/**
 * NB NB NB ! This code does not compile.
 * 
 * It showcases how 'const' is a compile-time guarantee that the value is not changed.
 **/
int main()
{
   int i = 3;   // initialize integer i to 3: OK
   const double pi = 3.14159; // initialize constant double pi to 3.14159: OK
   
   i = 8;    // assign 8 to i: OK
   pi = 3.0; // trying to reassign pi, but pi declared constant 
              // => COMPILATION ERROR
   
   return 0;
}
