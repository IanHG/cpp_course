#include <iostream>

int main()
{
   int  i[2];  // Array declaration
   int* pi;    // Pointer declaration
   int& ri = i[0]; // Reference declaration
   
   i[0] = 1.0; // Get element of (and assign to)
   i[1] = 2.0; // Get element of (and assign to)
   pi = &i[1]; // Get address of
   std::cout << *pi << std::endl; // Get value of

   return 0;
}
