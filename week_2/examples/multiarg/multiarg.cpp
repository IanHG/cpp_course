#include <iostream>

int sum(int i, int j) // declare as comma separated 'list'
{
  return i + j;
}

int main()
{
   int i1 = 2;
   int i2 = 3;
   int i3 = sum(i1, i2); // pass also as comma separated
   
   std::cout << i1 << " + " << i2 << " = " << i3 << std::endl;

   return 0;
}
