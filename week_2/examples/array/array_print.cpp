#include <iostream>

int main()
{
   char v[6]; // (static) array of size 6

   v[0] = 'a';
   v[1] = 'g';
   v[2] = 'n';
   v[3] = 'b';
   v[4] = 'v';
   v[5] = 'q';

   char* p;   // pointer to char

   p = &v[3]; // p now points to v's 4th element (we count from 0 ;) )

   char x = *p; // *p is the contents of p

   std::cout << x << std::endl;
}
