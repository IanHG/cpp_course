int main()
{
   char v[6]; // (static) array of size 6

   char* p;   // pointer to char

   p = &v[3]; // p now points to v's 4th element (we count from 0 ;) )

   char x = *p; // *p is the contents of p
}
