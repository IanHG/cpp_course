#include <iostream>

int square(int i) // argument is passed by value
                  // i.e. object is copied and passed to the function
{
   return i *= i;
}

void squareinplace(int& i) // argument is passed by reference
                           // i.e. object is not copied but passed
                           // directly to function
{
   i *= i;
}

int main()
{
   int i = 2, j = 3;
   int k = square(i);
   squareinplace(j);
   
   std::cout << i << std::endl; // will print 2, i is not modified
   std::cout << j << std::endl; // will print 9, j is squared in-place
   std::cout << k << std::endl; // will print 4, the square of i

   return 0;
}
