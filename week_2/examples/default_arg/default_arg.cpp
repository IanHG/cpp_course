#include <iostream>

int sum(int i, int j = 1) // if only one arguments is passed j is defaulted to 1
{
  return i + j;
}

int main()
{
   int i1 = 2; 
   int i2 = 3; 
   std::cout << sum(i1,i2) << std::endl; // will print 5
   std::cout << sum(i1)    << std::endl; // will print 3

   return 0;
}
