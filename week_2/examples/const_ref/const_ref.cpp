#include <iostream>

// pass by constant reference
int square(const int& i)
{         
   return i * i;
}

int main()
{
   int i = 2;
   int j = square(i);
   
   std::cout << i << " * " << i << " = " << j << std::endl;

   return 0;
}
