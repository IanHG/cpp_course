#include <iostream>

// squaring integers
int square(int x)
{
   return x * x;
}

// squaring doubles
double square(double x) // same name as above, now takes double argument 
{
  return x * x; 
}

int main()
{
   int i  = 2;
   int si = square(i); // calls integer square function
   
   double d  = 2.1;
   double sd = square(d); // calls double square function

   std::cout << si << std::endl;
   std::cout << sd << std::endl;

   return 0;
}
