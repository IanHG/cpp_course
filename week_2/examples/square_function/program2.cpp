#include<iostream>

// function for squaring integers
int square(int x); // only declare function (forward declare)

int main() // we make our main function after function declarations
{ 
   int x = 2;
   int y = square(x); // call function: y is now the square of x
   
   std::cout << "Square: " << y << std::endl;
   
   return 0;
}

// square function definition
int square(int x) // define function
{
   return x*x; // return square of input variable
}
