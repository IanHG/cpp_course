#include<iostream>

// function for squaring integers
int square(int x) // declare and define function
{
   return x * x; // return square of input variable
}

int main() // we make our main function after function declarations
{ 
   // program execution will start from here
   int x = 2;

   int y = square(x); // call function: y is now the square of x
   
   std::cout << "Square: " << y << std::endl;

   return 0;
}
