/**
 * Comment out 'double& ref;' on line 9 to compile
 **/
int main()
{
   double b = 2.1;
   double& bref = b; // bref is declared as a reference to b
   
   double& ref; // ref is not initialized so this will NOT compile
   
   int i = 5;   // assign value of 5 to i
   int& r = i;  // r is a reference to i
   int x = r;   // since r is reference to i assign value of 5 to x
   r = 2;       // since r is reference to i assign value of 2 to i

   return 0;
}
