#include <iostream>

int main()
{
   int  i  = 3;   // Declare integer 
   int* pi = &i;  // Declare pointer to integer
                  // and assign value to be the address of i

   std::cout << "i   = " << i   << std::endl;
   std::cout << "pi  = " << pi  << std::endl;
   std::cout << "*pi = " << *pi << std::endl;

   return 0;
}
