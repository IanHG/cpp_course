#include <iostream>

int main()
{
   int* p; // declare pointer to integer
   int i = 2;
   p = &i; // the value of p is now the address of i
           // i.e. p points to i
   int j = *p; // assign to j the value of the object pointed to by p
               // p points to i, so the value of i is assigned to j
   i = 3; // ...

   // what will get printed?
   std::cout << i   << std::endl;
   std::cout << j   << std::endl; 
   std::cout << *p << std::endl;
}
