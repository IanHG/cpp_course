#include <iostream>

int main()
{
   int* p = nullptr; // p now points to nothing
   
   if(p) // if p is set to nullptr, p will evaluate to false
   {
      std::cout << "not nullptr" << std::endl; // this code will not run!
   }
}
