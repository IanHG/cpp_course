#include <iostream>

int main()
{
   int i = 1337;
   int* p = &i;
   if(p) // p's value is the address of i, so p is not zero and will evaluate to true
   {
      std::cout << "value = " << *p << std::endl; // this code will be run
   }
}
