\documentclass[12pt,t,professionalfonts]{beamer}
\usetheme{CambridgeUS}

\input{header.tex}
\input{codetypeset.tex}
\input{ls.tex}

\begin{document}
\title{\cppname{} course}
\subtitle{Short introduction}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\titlepage
\end{frame}

\begin{frame}[fragile]
   {Where to get}

   Get the course from \href{https://gitlab.com/IanHG/cpp_course}{the Gitlab page}.

   \verticalspace

   Clone it locally with \iterminal{git}

   \begin{terminal}[Clone with SSH]
      git clone git@gitlab.com:IanHG/cpp_course.git
   \end{terminal}

   \begin{terminal}[Clone with HTTPS]
      git clone https://gitlab.com/IanHG/cpp_course.git
   \end{terminal}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Practical stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{frame}
%   {Practical stuff}
%   
%   The 3 WTFs
%   \begin{description}
%      \item[What]  \cppname{} course
%      \item[When]  Thursdays at 12-14
%      \item[Where] 1522-420 (in the Physics building)
%   \end{description}
%
%   Time frame
%   \begin{description}
%      \item[First time] Thurday 07 / 11 - 2019
%      \item[Last time]  To Be Decided (TBD) (probably 19 / 12 - 2019)
%   \end{description}
%
%   Who am I
%   \begin{description}
%      \item[Me!]
%         Ian Heide Godtliebsen
%      \item[Email]
%         ian@chem.au.dk
%   \end{description}
%\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The course
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   {Course}
   \begin{description}
      \item[Course topic]
         The course will teach features of the \cppname{} programming language.
         The course will focus on the \cppname{} syntax, 
         but many of the concepts taught here are easily generalizable to other languages.
      \item[Course format]
         %Each time I will hold a small lecture on something \cppname{}, and afterwards, 
         %as time permits, there will be a coding workshop with exercises in the material that was just presented.
         For now you will look at the slides and exercises yourself, and at your own pace.
      \item[Course material]
         The course material consists of lecture notes and exercises in {.pdf} format (your will need \iterminal{pdflatex} to compile the slides to \iterminal{.pdf}), 
         and code examples in plain text format. If you have trouble compiling the slides or exercises, let me know.
         %I will send out course material on a weekly basis.
      \item[Questions]
         If you have any questions just send an email in my direction!
   \end{description}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compiler
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {OS and Compiler}
   You need a working \cppname{} compiler to compile example and exercise code.
   
   I suggest using \href{https://gcc.gnu.org/}{\gppname{}}, which is easily installed on most Linux systems.

   \begin{description}
      \item[Linux]
         If you have a Linux box just install the \icpp{g++} compiler provided by your distributions package manager.
         \begin{terminal}[Ubuntu example]
$ sudo apt-get install g++
         \end{terminal}
      \item[Mac OS]
         Try to install \gppname{} using \href{http://www.edparrish.net/common/macgpp.php}{this} guide.
      \item[Windows]
         If you are on a Windoze box, I suggest installing \href{https://tutorials.ubuntu.com/tutorial/tutorial-ubuntu-on-windows#0}{Ubuntu for Windows}, 
         and following the Ubuntu guide above. Find out how to move files between Windows and Ubuntu filesystems.
   \end{description}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Editors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Editors}
   You need an editor to edit code files.
   
   The following are editors that can be used in the terminal (I know and use VIm myself, so I can be of help with that).
   \begin{itemize}
      \item
         \href{https://www.vim.org/}{VIm} (VI improved)
      \item
         \href{https://www.gnu.org/software/emacs/}{Emacs}
      \item
         \href{https://www.nano-editor.org}{GNU nano}
   \end{itemize}

   If you would like a graphical editor/IDE instead, here are some examples.
   \begin{itemize}
      \item
         \href{https://code.visualstudio.com/}{Visual Studio Code}
      \item
         \href{https://www.eclipse.org/}{Eclipse}
      \item
         \href{https://www.sublimetext.com/}{Sublime}
   \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Internet resources
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\emph{Google-fu} and resources}
   Some internet resources for when we start coding.

   \begin{itemize}
      \item
         \href{https://google.com}{Google} is your friend! The ability to search up information is an invaluable tool when learning to code. This skill is known as \emph{Google-Fu}.
      \item
         \href{https://stackoverflow.com/}{Stack overflow} is a great resource. You will often find your answers here. Make an account, ask questions, and up-vote good answers.
      \item
         \href{https://en.cppreference.com/w/}{cppreference} to look up STL function descriptions and signatures.
      \item
         \href{http://www.cplusplus.com/}{cplusplus} same as the above.
      \item
         \href{https://projecteuler.net/}{Project Euler} is a collection of interesting programming problems. 
         Great if you want to exercise your brain, and try to use some of the stuff you've learned in the course.
   \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test your setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Test your setup}
   
   Check that you can open and edit the \icpp{main.cpp} file in your editor and that you can compile it with your \icpp{g++} compiler.

   \includecpp{main.cpp}

   \begin{terminal}
$ g++ main.cpp
$ ./a.out
It's working!
   \end{terminal}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% C++ is hard
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   {Learning \cppname}
   \begin{center}
      \includegraphics[scale=0.45]{../figures/learn_cpp_in_21_days.jpg}
   \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Philosophy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   {Course Philosphy}
   
   \verticalspace
   \verticalspace
   \verticalspace
   
   \inspirationalquote
   {
      A teacher is never a giver of truth; he is a guide, a pointer to the truth that each student must find for himself. A good teacher is merely a catalyst.
   }
   {  
      Bruce Lee
   }

\end{frame}

\begin{frame}
   {Who am I}

   \begin{description}
      \item[Me!]
         Ian Heide Godtliebsen
      \item[Email]
         ian@chem.au.dk
   \end{description}

\end{frame}

\end{document}
