\documentclass[t]{beamer}
\usetheme{CambridgeUS}

\input{header.tex}
\input{codetypeset.tex}
\input{ls.tex}

\begin{document}
\title{\cppname{} move semantics}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   \titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Today}

   Today we will cover:

   \begin{itemize}
      \item
         \emph{l-values} and \emph{r-values}
      \item
         R-value references and move operators
      \item
         Return Value Optimization (RVO)
   \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Objects and values}

   An \emph{object} is a contiguous region of memory \\
   \verticalspace
   We define an \emph{lvalue} as an expression that refers to an object \\
   \verticalspace
   We call it an \emph{lvalue} because it can stand to the left of \icpp{=} \\
   \verticalspace
   We can always get the address of an \emph{lvalue} by using the \icpp{&} operator \\
   \verticalspace
   
   \includecpplines[lvalue]{examples/lvalue/main.cpp}{3}{6}%

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   { Complementing \emph{lvalues}: \emph{rvalues} }

   \emph{rvalues} roughly means: value that is not an \emph{lvalue} (!)\\
   \verticalspace
   They can only stand to the right of assignment \icpp{=} \\
   \verticalspace
   These are \emph{e.g.} {\bf temporary} values (function returns), literal constants, \emph{etc.}. \\
   \verticalspace
   
   \includecpplines[lvalue]{examples/rvalue/main.cpp}{3}{8}%
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Advanced \emph{lvalues} and \emph{rvalues} }

   Two properties of objects matter: \\
   \verticalspace
   {\bf Has identity:} The object has identity, \emph{i.e.} it has a name or a pointer. \texttt{\{\textcolor{blue}{i}\}}\\
     \verticalspace
     {\bf Movable:} The object may be moved to another memory location. \texttt{\{\textcolor{blue}{m}\}}
   \verticalspace
   \begin{center}
     \includegraphics[scale=1]{figures/values/values-figure0.pdf}
   \end{center}
   \verticalspace
   \texttt{glvalue:} \emph{ generalized lvalue} \\
   \texttt{prvalue:} \emph{ pure rvalue}, \emph{e.g.} temporaries returned from functions \\ 
   \texttt{xvalue: } \emph{'extraordinare'/'expert' or 'expiring' value}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\emph{Rvalue} reference}

   \emph{Rvalue} reference refers to a 'temporary' object \\
   \verticalspace
   In \cppname{}11 declare an rvalue reference using the declarator \icpp{&&} \\
   \begin{cpp}
      int&& i; <-- is a temporary an can be modified without consequence
   \end{cpp}
   \verticalspace
   Object can safely be modified extensively, even 'destroyed' \\
   \verticalspace
   Maybe counter-intuitively an rvalue reference is actually an lvalue, as it has identity 
   (here we call it \icpp{i}) \\

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\emph{Rvalue} reference example}
   
   \includecpplines[Function returning lvalue]{examples/rvalue_reference/main.cpp}{4}{4}%
   
   \includecpplines[R-value- and l-value-references]{examples/rvalue_reference/main.cpp}{8}{18}%

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\emph{Rvalue} reference as function argument}
   
   \includecpplines[Rvalue reference argument]{examples/rvalue_reference_func/main.cpp}{3}{7}
   \includecpplines[Usage]{examples/rvalue_reference_func/main.cpp}{11}{15}
   \verticalspace
   {\bf NB:} Function takes rvalue reference as argument, but inside \icpp{func} the \icpp{str} variable is an \emph{lvalue}!
   \emph{i.e.} as it has identity (name)
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Swap meet}

   Sometimes programmers know an object will not be used again. Consider:
   
   \includecpplines[Old style swap]{examples/swap/main.cpp}{4}{9}

   This is very ineffecient as we make three (possibly expensive) copies. \\

   \verticalspace

   It would be better if we could just exchange the data in the objects without copying. \\
   
   %\includecpplines[New style swap]{examples/swap/main.cpp}{12}{17}
   
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\icpp{T&&} and \icpp{std::move} }

   \icpp{static_cast<T&&>(x)} returns an xvalue, which references \icpp{x} \\ 
   \verticalspace
   Using \icpp{static_cast<T&&>(x)} is a little verbose. \\
   \verticalspace
   We can use \icpp{std::move} defined in header \icpp{utility}. \\
   
   \begin{cpp}[STL \icpp{move}]
      template <typename T>
      typename remove_reference<T>::type&& move(T&& arg)
      {
         return static_cast<typename remove_reference<T>::type&&>(arg);
      }
   \end{cpp}
   \verticalspace
   With this we can do \icpp{std::move(x)}, which expresses nicely our intention \\
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Pretty swap function using \icpp{move} }

   We can now make an "almost perfect" swap function \\
   \verticalspace
   
   \includecpplines[Swapping lvalues]{examples/swap/main.cpp}{20}{26}

   \verticalspace
   This is essentially the same implementation as \icpp{std::swap} in the STL \\
   \verticalspace
   \icpp{std::swap} is also defined in the \icpp{utility} header. \\
   \verticalspace
   With this we can swap two objects of the same type with little overhead \\

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\cppname{} move operators}

   Initializing or assigning from temporary object, means no need to copy \\
   \verticalspace
   Temporary will get destroyed immediately after $\rightarrow$ steal the data instead! \\
   \verticalspace
   With \emph{rvalue} reference we got {\bf move} semantics \\
   \verticalspace
   \includecpplines[]{examples/vector/Vector.hpp}{5}{15}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {C-tor implementations}

   \includecpplines[]{examples/vector/Vector.cpp}{5}{21}
   {\bf NB:} Move must always leave moved object in a destructible state!
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Move in action}
   
   \includecpplines[]{examples/vector/main.cpp}{12}{24}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Reference collapsing rules}
   
   \includecpplines[]{examples/ref_collapsing_rules/main.cpp}{23}{25}

   \begin{center}
      \renewcommand{\arraystretch}{0.95}
      \small
      \begin{tabular}{|c|c|c|}
         \hline
         \rowcolor{Gray}
         Type \icpp{T} & Type \icpp{TR}  & Type of \icpp{var} \\ 
         \rowcolor{LightCyan}
         \icpp{A}      & \icpp{T}        & \icpp{A}           \\
         \rowcolor{LightCyan}
         \icpp{A}      & \icpp{T&}       & \icpp{A&}          \\
         \rowcolor{LightCyan}
         \icpp{A}      & \icpp{T&&}      & \icpp{A&&}         \\ 
         \rowcolor{LightGreen}
         \icpp{A&}     & \icpp{T}        & \icpp{A&}          \\
         \rowcolor{LightGreen}
         \icpp{A&}     & \icpp{T&}       & \icpp{A&}          \\
         \rowcolor{LightGreen}
         \icpp{A&}     & \icpp{T&&}      & \icpp{A&}          \\ 
         \rowcolor{LightCyan}
         \icpp{A&&}    & \icpp{T}        & \icpp{A&&}         \\
         \rowcolor{LightCyan}
         \icpp{A&&}    & \icpp{T&}       & \icpp{A&}          \\
         \rowcolor{LightCyan}
         \icpp{A&&}    & \icpp{T&&}      & \icpp{A&&}         \\
         \hline
      \end{tabular}
   \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Return Value Optimization(RVO)}
   
   RVO is a compiler optimization technique eliminating the temporary object created by a function's return value. \\
   \verticalspace
   The \cppname{} standard allows a compiler to perform optimization, 
   provided the resulting executable exhibits the same observable behaviour. \\
   \verticalspace
   RVO is special case in the \cppname{} standard: 
   a compiler implementation may omit a copy operation resulting from a return statement, 
   even if the copy constructor has side effects. \\
   \verticalspace
   {\bf Thus} In \cppname{}, RVO is allowed change observable behaviour of the resulting program!!! \\
   \verticalspace
   {\bf Best practice:} Do not let copy constructors have side-effects!

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {RVO example}
   
   Consider the following class definition

   \includecpplines[RVO test class]{examples/rvo/RVO.h}{7}{20}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {RVO example continued}

   What will get printed byt the following program? \\
   \verticalspace
   Why does the program print what it prints?
   \verticalspace

   \includecpp[RVO in action]{examples/rvo/main.cpp}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Compile with \iterminal{g++} and run}

   Lets test!

   \begin{terminal}[RVO in action]
      $ g++ main.cpp -o rvo.x
      $ ./rvo.x
       default ctor
   \end{terminal}
   
   \begin{terminal}[Disable RVO]
      $ g++ -fno-elide-constructors main.cpp -o no_rvo.x
      $ ./no_rvo.x
       default ctor 
       copy ctor 
       copy ctor
   \end{terminal}
   \verticalspace
   RVO and constructor eliding saves a lot of computational effort! \\
   \verticalspace
   {\bf BUT:} The two executables behave differently if copy ctor has side-effects! \\
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{That's all folks!}
  \begin{center}
    \large
      \verticalspace
      Now crack some code! \\
      \verticalspace
      Compile everything with flag \iterminal{-std=c++11} or later!
  \end{center}
\end{frame}
\end{document}
