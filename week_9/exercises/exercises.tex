\documentclass[]{article}

\input{header.tex}
\input{codetypeset.tex}

\begin{document}
\normalsize
\begin{center}
\bf\large Exercises Week 9
\end{center}
\begin{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf \emph{lvalues} and \emph{rvalues} }

   Which of the following statements are unambiguously \icpp{true}/\icpp{false}? 
   When you are done check the hint ;)
   \begin{itemize}
     \item
       \emph{lvalues} can stand to the right of an \icpp{=}.
     \item
       \emph{rvalues} can stand to the right of an \icpp{=}.
     \item
       \emph{rvalues} are moveable.
     \item
       An \emph{lvalue} is also an \emph{rvalue}.
     \item
       An \emph{lvalue} is something that is not an \emph{rvalue}.
     \item
       An \emph{xvalue} is an \emph{rvalue}.
     \item
       A \emph{prvalue} has identity.
     \item
       An object that has identity cannot be moved.
   \end{itemize}
   \hiddenhint{ true, true, true, false, false, true, false, false. }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf \emph{lvalue} and \emph{rvalue} references}

   Go to \iterminal{examples/rvalue_refence} and open \iterminal{main.cpp}.
   
   Comment out all lines that will give a compilation error.

   In turn comment in each line that that will produce an error, compile the program,
   and make sure you understand the compilation error and 
   why the given line generates a that error.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\item{\bf Weird recursion}

   What will the following program print and why?
   
   \includecpp[Very weird indeed!]{weird_recursion/main.cpp}

   \hiddenhint{It will call a different \icpp{recursive_func} depending on whether the argument is an \emph{lvalue} or an \emph{rvalue}.}

   \hiddenhint{The answer is \iterminal{28}.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf Swapping}

  Implement the last \icpp{swap()} function given in the slides.

  Implement the two other \icpp{swap()}'s with signatures 
  \icpp{template<class T> void swap(T& a, T&& B)} and
  \icpp{template<class T> void swap(T&& a, T& B)}.

  \hiddenhint{ They can have the same implementation as the one in the slides. 
  There is however a more optimized way. }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\item{\bf \icpp{Vector} class }
   
   Go to \iterminal{examples/vector} and check that each case 
   calls the correct constructor or assignment operator.

   \includecpplines[]{../examples/vector/main.cpp}{12}{24}

   \hiddenhint{Make each constructor or assignment operator print a unique message.}

   Make a constructor for \icpp{Vector} that takes a size and allocates the \icpp{elements} pointer.

   Make a program that \icpp{swap}'s two \icpp{Vector}'s, 
   and print the \icpp{elements} pointer of both vectors
   before and after the swap (print the actual pointer value (address) and not the elements themselves).

   Now make a program that tries to swap using the \icpp{swap_old} implementation,
   and print the \icpp{elements} pointers before and after.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\item{\bf Updating the \icpp{Matrix} class }

   Using the \icpp{Vector} example as a guide, 
   update your \icpp{Matrix} class to have at least the following functions

   \begin{itemize}
      \item
         default constructor
      \item
         copy constructor
      \item
         copy assignment
      \item
         move constructor
      \item
         move assignment
   \end{itemize}

   {\bf NB:} Remember that when moving an object, 
   the moved object must be left in a destructible state.

   Make a test program that tests/calls each of these functions.

   \hiddenhint{If you have trouble testing your move constructor, you can try turning off RVO.}

   Try to \icpp{swap} two matrices using the implementation of almost "perfect \icpp{swap}",
   and check that no copies are made.

   \hiddenhint{You can check by printing something from your constructors and assignment operators of \icpp{Matrix}.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf Reference collapsing }

   Compile and run \iterminal{examples/ref_collapsing_rules/main.cpp},
   and make sure the table outputtet here is the same as given in the
   slides.

   In each of the following cases what type is variable \icpp{a}?:
   \begin{cpp}[Case a)]
      using T = int&;
      T&& a;  // <-- What is type of 'a'?
   \end{cpp}

   \begin{cpp}[Case b)]
      template <typename T> void func(T&& a);
      auto fp = func<double&&>; // <-- What is type of 'a' in the 'fp' function signature?
   \end{cpp}

   \begin{cpp}[Case c)]
      using T1 = float;
      using T2 = T1&&;
      using T3 = T2&;
      T3& a; // what is type of 'a' ?
   \end{cpp}

   \hiddenhint{\icpp{int&}, \icpp{double&&}, \icpp{float&} }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\item{\bf \emph{Universal references} and \emph{perfect forwarding} }

   \emph{NB: This exercise dives into somethings that can be a little confusing, but also very powerful once you've mastered it!}

   Consider a function \icpp{func_ref} that takes two arguments \icpp{t} and \icpp{u}
   and the wrapper function
   \includecpplines[Function]{perfect_forwarding/main.cpp}{17}{22}
   This wrapper can do some stuff (\emph{e.g.} some logging) and 
   then pass the arguments on to the function. This however has a problem that 
   we cannot pass r-values, as a normal reference (\icpp{&}) cannot bind an r-value.
   We can create another implementation taking values instead of references
   \includecpplines[Function]{perfect_forwarding/main.cpp}{24}{29}
   but, this will have the problem that if \icpp{func_ref} makes changes to \icpp{t} and \icpp{u},
   these changes will only be applied to the copies inside the wrapper, 
   and not the objects back in the calling code.
   \includecpplines[Using the wrappers]{perfect_forwarding/main.cpp}{56}{59}
   Comment in each of above lines, compile the code and make sure you understand why it compiles or not
   and what gets printed.

   To solve this issue we can use \emph{perfect forwarding}.
   \emph{Perfect forwarding} is a way to perfectly forward arguments, handling both l-value and r-value references, 
   \emph{e.g.} when forwarding arguments from a wrapper function to another function.
   Consider the following function template 
   \includecpplines[]{perfect_forwarding/main.cpp}{32}{33}
   Here \icpp{T&&} and \icpp{U&&} are known as \emph{universal references} or \emph{forwarding references}
   (both names have been used in the litterature).
   What is \icpp{T} and \icpp{U} deduced as when passing an l-value reference? when passing an r-value reference (you will check your assumptions later in this exercise)?
   
   Using \icpp{std::forward} we can write a wrapper that perfectly forwards arguments to this functions,
   regardless of whether the argument is an l-value or an r-value.
   \includecpplines[Using \icpp{std::forward}]{perfect_forwarding/main.cpp}{44}{49}
   
   The implementation of \icpp{std::forward} is straight-\emph{forward} (pun-intended),
   and there are just two overloads, one for l-value reference and one for r-value reference:
   \begin{cpp}[Implementation of \icpp{std::forward}]
      template<class T>
      T&& forward(typename std::remove_reference<T>::type& t) noexcept 
      {
         return static_cast<T&&>(t);
      }

      template <class T>
      T&& forward(typename std::remove_reference<T>::type&& t) noexcept 
      {
         return static_cast<T&&>(t);
      }
   \end{cpp}
   This means that if \icpp{std::forward} is passed an l-value reference, it returns an l-value reference (due to reference collapsing rules).
   If \icpp{std::forward} is passed an r-value reference with identity (which is in fact an l-value) it will return an xvalue,
   which can be moved (equivalent to using \icpp{std::move}). Using this we can perfectly forward arguments!

   We can now call our \icpp{wrapper} with both l-values and r-values.
   \includecpplines[]{perfect_forwarding/main.cpp}{61}{62}
   Comment in each of the lines, compile the program and run, 
   and make sure you know what gets printed and why.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\item{\bf Forwarding \icpp{Matrix}'es }

   Make a function that takes a \icpp{Matrix} and multiplies all elements of the matrix with a number,
   returning the result as a new \icpp{Matrix}. The function signature could be something like
   \begin{cpp}
      Matrix multiply(const Matrix& mat, double factor);
   \end{cpp}

   Now make an overload such that if the function is called with a temporary, no copies are made.
   Check that this works.

   Now make a wrapper function that prints/logs the matrix size, and the factor, then calls \icpp{multiply} and returns the result.
   You should of course perfectly forward the arguments between the wrapper and \icpp{multiply}.
   Check that your perfect forwarding works.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf RVO}

  Go to \iterminal{examples/rvo}.

  Compile the program both with and without \iterminal{-fno-elide-constructor},
  and make sure you know why the two executables print what they print.

  Implement both a move constructor and move assignment operator that like the other functions
  prints a unique message to the terminal.
  Compile the program again with and without \iterminal{-fno-elide-constructor},
  and see what gets printed.

  Usage of RVO is compiler dependent, so you might see different outputs for different compilers.

  Why should a constructor not have side effects?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf Zen-Master}

  You are now a master in \emph{lvalues}, \emph{rvalues}, and \cppname{} move semantics.
  So \emph{move} over to \iterminal{https://projecteuler.net/} and apply your new skillset!

\end{enumerate} 

\end{document}
