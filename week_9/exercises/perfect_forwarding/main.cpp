#include <iostream>
#include <type_traits>

#include "type_name.hpp"

// Function taking reference
template<class T, class U>
void func_ref(T& d, U& i)
{
   static_assert(std::is_floating_point_v<T>, "T must be floating point.");
   static_assert(std::is_integral_v      <U>, "U must be integer.");

   d = 2.1;
   i = 3;
}

template<class T, class U>
void wrapper_ref(T& t, U& u)
{
   // ... do some wrapper stuff ...
   func_ref(t, u);
}

template<class T, class U>
void wrapper_val(T t, U u)
{
   // ... do some wrapper stuff ...
   func_ref(t, u);
}

// Function taking universal/forwarding reference
template<class T, class U>
void func(T&& d, U&& i)
{
   static_assert(std::is_floating_point_v<std::remove_reference_t<T> >, "T must be floating point.");
   static_assert(std::is_integral_v      <std::remove_reference_t<U> >, "U must be integer.");

   std::cout << "T : " << type_name<T>() << "   U : " << type_name<U>() << std::endl;

   d = 2.1;
   i = 3;
}

template<class T, class U>
void wrapper(T&& t, U&& u)
{
   // ... do some wrapper stuff ...
   func(std::forward<T>(t), std::forward<U>(u));
}

int main()
{
   double d = 1.0;
   int    i = 2;

   //wrapper_val(d, i);  // <-- Compiles, but does not modify d and i
   //wrapper_ref(d, i);  // <-- Compiles
   //wrapper_val(d, 4);  // <-- Compiles, but does not modify d 
   //wrapper_ref(d, 4);  // <-- Does not compile as U& cannot bind the rvalue 4
   
   //wrapper(d, i); // <-- Compiles, and correctly modifies d and i
   //wrapper(d, 4); // <-- Compiles, and correctly modifies d
   
   std::cout << " d = " << d << "  i = " << i << std::endl;
}
