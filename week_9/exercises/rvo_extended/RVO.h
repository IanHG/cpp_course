#ifndef RVO_H_INCLUDED
#define RVO_H_INCLUDED
#include <iostream>

class RVO { // class for testing RVO principles
   public:
      RVO() { // default ctor
         std::cout << " default ctor " << std::endl;
      }

      RVO(const RVO& rvo) { // copy ctor
         std::cout << " copy ctor " << std::endl;
      }
      
      RVO(RVO&& rvo) { // copy ctor
         std::cout << " move ctor " << std::endl;
      }
      
      RVO& operator=(const RVO& rvo) { // copy assignment
         std::cout << " operator= " << std::endl;
      }
};

#endif /* RVO_H_INCLUDED */
