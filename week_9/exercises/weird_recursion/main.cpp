#include <iostream>
#include <utility> // for std::move

int recursive_func(int&& i); // forward declare

int recursive_func(int& i)
{
   if(!(i%7))
   {
      return i;
   }
   else
   {
      i+=2;
      return recursive_func(std::move(i));
   }
}

int recursive_func(int&& i)
{
   if(!(i%17))
   {
      return i;
   }
   else
   {
      i+=3;
      return recursive_func(i);
   }
}

int main()
{
   std::cout << recursive_func(5) << std::endl;
   return 0;
}
