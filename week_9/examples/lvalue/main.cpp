int main()
{
   int i; // i is an lvalue
   i = 2; // it can stand to the left of =
   int* pi = &i; // we can take the address of i

   return 0;
}
