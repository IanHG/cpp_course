#include <iostream>
#include <iomanip>

#include "type_name.hpp" // 'type_name' to print types

// Empty dummy class
class A
{
};

A a; // Dummy object to initialize a reference

// Pretty printing.
void print_line(const std::string& a, const std::string& t, const std::string& type)
{ 
   int width = 5;
   std::cout << std::left << std::setw(width) << a << std::setw(width) << t << std::setw(width) << type << std::endl;
}

// Function to test collapsing "A& &"
void A_lref_lref()
{
   using T  = A&; // Define a type 'T'
   using TR = T&; // Define as a reference type based on 'T'
   TR var = a;    // <-- What type is 'var'?

   print_line("A&", "T&", type_name<TR>());
}

// Define a macro to test reference collapsing
#define COLLAPSE_TEST(AA, TT) \
   { \
      using T  = AA; \
      using TR = TT; \
      print_line(#AA, #TT, type_name<TR>()); \
   }

int main()
{
   A_lref_lref();

   std::cout << "\nTable:\n";

   COLLAPSE_TEST(A, T);
   COLLAPSE_TEST(A, T&);
   COLLAPSE_TEST(A, T&&);
   COLLAPSE_TEST(A&, T);
   COLLAPSE_TEST(A&, T&);
   COLLAPSE_TEST(A&, T&&);
   COLLAPSE_TEST(A&&, T);
   COLLAPSE_TEST(A&&, T&);
   COLLAPSE_TEST(A&&, T&&);
}
