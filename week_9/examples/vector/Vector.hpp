#pragma once
#ifndef VECTOR_HPP_INCLUDED
#define VECTOR_HPP_INCLUDED

class Vector {
   public:
      Vector() = default;
      Vector(const Vector& vec); // copy ctor: copy vec's representation
      Vector(Vector&& vec); // move ctor: we may steal vec's representation
      Vector& operator=(const Vector& vec); // copy assignment: copy vec's representation
      Vector& operator=(Vector&& vec); // move assignment: we may steal vec's representation
   private:
      int     size = 0;
      double* elements = nullptr;
};

#endif /* VECTOR_HPP_INCLUDED */
