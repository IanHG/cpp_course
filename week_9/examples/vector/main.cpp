#include "Vector.hpp"

Vector vector_func() // declare function returning Vector
{
   return Vector{};
}

// ... some code ....

int main()
{
   // 1 
   Vector vec = vector_func(); // vector_func() is an rvalue. will call move ctor
   
   // 2
   Vector vec2;
   vec2 = vector_func(); // vector_func() is an rvalue. call move assignment
   
   // 3
   Vector vec3 = static_cast<Vector&&>(vec2); // make vec2 an rvalue. use move ctor
   
   // 4
   Vector vec4;
   vec4 = vec3; // vec is lvalue, will call copy assignment

   return 0;
}
