#include "Vector.hpp"

#include <utility> // for std::swap

// copy ctor, argument is const ref
Vector::Vector(const Vector& vec) 
   :  size(vec.size)
   ,  elements(nullptr) 
{
  elements  = new double[size]; // allocate new memory to hold copy
  for(int i=0; i < size; ++i) elements[i] = vec.elements[i]; // copy elements
}

// move ctor, argument is rvalue ref
Vector::Vector(Vector&& vec)
   :  size(vec.size)
   ,  elements(nullptr) 
{
   std::swap(elements, vec.elements); // no copy, we just swap the pointers!
   vec.size = 0; // other vec now has size 0 (points to nullptr)
}

// copy assignment
Vector& Vector::operator=(const Vector& vec)
{
   if(elements)
      delete[] elements; // delete old pointer to not leak memory
   size = vec.size;
   elements = new double[size]; // allocate new pointer to hold all the data from vec
   for(int i=0; i < size; ++i) elements[i] = vec.elements[i]; // copy elements
   return *this;
}

// move assignment
Vector& Vector::operator=(Vector&& vec)
{ 
   // We can just swap all elements as vec is rvalue reference as thus not used after this function call
   std::swap(size, vec.size);
   std::swap(elements, vec.elements);
   return *this;
}
