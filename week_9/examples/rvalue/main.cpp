int main()
{
   int i;
   i = 2; // 2 is an rvalue, it can stand to right of =
   
   // silly stuff...
   2 = i; // 2 cannot stand to the left of =, makes no sense! WILL NOT COMPILE!
   int* pi = &2; // 2 is not in memory, i.e. cannot take ref: WILL NOT COMPILE!
   
   return 0;
}
