#include <string>
#include <iostream>

std::string func() { return "Organic"; } // declare function returning string

int main()
{
   std::string str {"TeoChem"}; // initialize string object
   
   std::string& rstr1 {str};      // lvalue reference, OK: rstr1 refers to str (lvalue)
   std::string& rstr2 {func()};   // lvalue reference, ERROR: func() is an rvalue
   std::string& rstr3 {"Biomod"}; // lvalue reference, ERROR: cannot bind to temporary
   
   std::string&& rrstr1 {func()};  // rvalue reference, OK: bind rrstr1 to temporary (rvalue)
   std::string&& rrstr2 {str};     // rvalue reference, ERROR: str is an lvalue
   std::string&& rrstr3 {"qLEAP"}; // rvalue reference, OK: refer to temporary
   
   const std::string& crstr {"MidasCpp"}; // OK! const reference binds temporary!
   
   std::cout << str << std::endl;

   std::cout << rstr1 << std::endl;
   
   std::cout << rrstr1 << std::endl;
   std::cout << rrstr3 << std::endl;
   
   std::cout << crstr << std::endl;

   return 0;
}
