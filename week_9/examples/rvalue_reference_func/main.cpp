#include <string>

std::string func(std::string&& str) // argument is rvalue reference
{
  // ... manipulate str ...
  return str;
}

int main()
{
   func("I R rvalue!"); 
   func(func("I R also rvalue!")); // func return rvalue: OK!
   
   std::string str_lval = "I am not an rvalue";
   func(str_lval); // str_lval is lvalue: COMPILATION ERROR!

   return 0;
}
