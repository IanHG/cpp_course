#include<utility> // for std::move

// Old style swap
template<class T>
void swap_old(T& a, T& b) { // inefficient swap (many copies)
   T temp {a}; // copy a into temp
   a = b;      // copy b into a
   b = temp;   // copy temp (a) into b, we have completed our swap
}

// New style swap (verbose)
template<class T>
void swap_new(T& a, T& b) { // almost "perfect swap"
   T temp {static_cast<T&&>(a)}; // initialization may modify a
   a = static_cast<T&&>(b);      // assignment may modify b
   b = static_cast<T&&>(temp);   // assignment may modify temp
}

// New swap using move
template<class T>
void swap(T& a, T& b) // almost 'perfect swap' (no copies!)
{
   T temp = std::move(a); // move a to temp using  move ctor
   a = std::move(b);      // move b to a using move assignment
   b = std::move(temp);   // move temp (a) to b using move assignment, swap complete 
}

template<class T>
void swap(T& a, T&& b); // swap with rvalue

template<class T>
void swap(T&& a, T& b); // swap with rvalue

int main()
{
   return 0;
}
