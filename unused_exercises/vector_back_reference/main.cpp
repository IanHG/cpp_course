#include <iostream>
#include <vector>

int main()
{
   std::vector<double> vec;

   vec.emplace_back(2);
   double& d1 = vec.back();

   vec.emplace_back(3);
   double& d2 = vec.back();

   std::cout << d1 << std::endl;
   std::cout << d2 << std::endl;

   return 0;
}
