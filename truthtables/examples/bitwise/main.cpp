#include <iostream>
#include <typeinfo> // for typeid
#include <bitset>

int main()
{
   int i = 225;
   int j = 113;
   
   std::cout << "type of i     : " << typeid(i).name() << std::endl;
   std::cout << "type of j     : " << typeid(j).name() << std::endl;
   std::cout << "type of i & j : " << typeid(i & j).name() << std::endl;
   std::cout << "type of i ^ j : " << typeid(i ^ j).name() << std::endl;

   int k = i & j;

   std::cout << i << " & " << j << " = " << k << std::endl;
   
   // Integer is 32-bit, but we just print the first 8 bits for clarity
   std::cout << std::bitset<8>(i) << std::endl;
   std::cout << std::bitset<8>(j) << std::endl;
   std::cout << std::bitset<8>(k) << std::endl;
   
   int l = i ^ j;

   std::cout << i << " ^ " << j << " = " << l << std::endl;
   
   // Integer is 32-bit, but we just print the first 8 bits for clarity
   std::cout << std::bitset<8>(i) << std::endl;
   std::cout << std::bitset<8>(j) << std::endl;
   std::cout << std::bitset<8>(l) << std::endl;

   return 0;
}
