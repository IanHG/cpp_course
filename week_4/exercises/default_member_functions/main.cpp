class X
{
   private:
      X();
};

class Y
{
   public:
      Y(int);
};

class Z
{
   public:
      Z() = delete;
};

int main()
{
   X x;
   Y y;
   Z z;
   return 0;
}
