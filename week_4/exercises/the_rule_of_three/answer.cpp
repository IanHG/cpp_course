class ptr_holder
{
   private:
      double* m_ptr;
   public:
      ptr_holder(): m_ptr(new double[1000])
      {
      }
   
      // Copy constructor
      ptr_holder(const ptr_holder& other)
         :  m_ptr(new double[1000]) // Allocate pointer for our new object
      {
         // Loop over 'other's pointer and assign value to this objects pointer
         for(int i = 0; i < 1000; ++i) 
         {
            m_ptr[i] = other.m_ptr[i];
         }
      }

      // Copy Assignment
      ptr_holder& operator=(const ptr_holder& other)
      {
         if(this != &other) // Check for self assignment
         {
            for(int i = 0; i < 1000; ++i)
            {
               this->m_ptr[i] = other.m_ptr[i];
            }
         }
         return *this; // Remember to return a reference to object, so we can chain operator=
      }

      ~ptr_holder()
      {
         delete[] m_ptr;
      }
};

int main()
{
   ptr_holder h1;
   ptr_holder h2(h1);
   ptr_holder h3;
   h3 = h2;

   return 0;
}
