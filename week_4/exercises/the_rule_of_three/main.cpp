class ptr_holder
{
   private:
      double* m_ptr;

   public:
      ptr_holder(): m_ptr(new double[1000])
      {
      }

      ~ptr_holder()
      {
         delete[] m_ptr;
      }
};

int main()
{
   ptr_holder h1;
   ptr_holder h2(h1);
   ptr_holder h3;
   h3 = h2;
   
   return 0;
}
