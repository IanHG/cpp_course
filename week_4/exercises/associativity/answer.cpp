#include <iostream>
#include <string>

class assoc_type
{
   private:
      std::string m_name;

   public:
      assoc_type(const std::string& name)
         :  m_name(name)
      {
      }

      friend std::ostream& operator<<(std::ostream& os, const assoc_type& at);

      friend assoc_type operator+(const assoc_type& lhs, const assoc_type& rhs);
};

std::ostream& operator<<(std::ostream& os, const assoc_type& at)
{
   os << at.m_name;
   return os;
}

assoc_type operator+(const assoc_type& lhs, const assoc_type& rhs)
{
   assoc_type add("(" + lhs.m_name + " + " + rhs.m_name + ")");
   return add;
}

int main()
{
   assoc_type a("a");
   assoc_type b("b");
   assoc_type c("c");
   assoc_type d("d");

   assoc_type sum = a + b + c + d;

   std::cout << sum << std::endl;

   return 0;
}
