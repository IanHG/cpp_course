#include <iostream>

class X
{
  private:
    int m_x = 2;

  public:
    int Getx1() const
    {
      return m_x; // return member m_x
    }

    int Getx2() const
    {
      return this->m_x; // access m_x through this, does the same as Getx1() 
    }
};

int main()
{
   X x;

   std::cout << x.Getx1() << std::endl;
   std::cout << x.Getx2() << std::endl;

   return 0;
}
