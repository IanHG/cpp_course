class defaulting
{
   public:
      defaulting() = default; // default 'default'-constructor
      defaulting(const defaulting&) = default; // default copy constructor
      defaulting& operator=(const defaulting&) = default; // default copy constructor
      ~defaulting() = default; // default destructor
};

int main()
{
   defaulting d1; // Ok, 'default' contructor is default
   defaulting d2(d1); // Ok, copy constructor is default
   defaulting d3;

   d3 = d1;  // Ok, copy assignment is default
      
   return 0;
}
