#include <iostream>

int main()
{
   int i = 8;
   int j = 3;
   int largest = i > j ? i : j;

   std::cout << largest << std::endl;

   return 0;
}
