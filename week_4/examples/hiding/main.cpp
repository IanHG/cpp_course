/**
 * NB ! NB ! NB! This program will not compile!
 **/

class X_hidden
{
   private:
      X_hidden(); // default constructor is now private. cannot be invoked outside class
};

class X_deleted
{
   public:
      X_deleted() = delete; // i have now deleted the default constructor
};

int main()
{
   /* None of the below two lines will compile */ 
   X_hidden  hidden;   
   X_deleted deleted;

   return 0;
}
