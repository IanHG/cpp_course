double add(double lhs, double rhs)
{
   return lhs + rhs;
}

void add_assign(double& lhs, double rhs)
{
   lhs = lhs + rhs;
}

double minus(double arg)
{
   return -arg;
}

int main()
{
   double x = 1.0;
   double y = 2.0;

   double z;

   // addition
   z = add(x,y); // we can probable guess what this does
   
   z = x + y;  // but we most certainly know by heart what this does!
   
   // add assignment
   add_assign(x, y); // what about this?.. where do we assign ??...
   
   x += y;  // here we see it easily, we of course assign to x
   
   // unary minus
   z = minus(x); // easy to understand..
   
   z = -x; // but this is more compact

   return 0;
}
