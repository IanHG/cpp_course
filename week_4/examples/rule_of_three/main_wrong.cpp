/**
 * NB ! THIS EXAMPLE IS A LITTLE CONTRIVED (and actually not that great...)
 *
 * Do not put too much emphasis on why stuff is done as it is, just note that
 * 'non_trivial_type' is not trivially copyable, 
 * and we thus have to implement copy asssigment and copy constructor.
 * We don't actually need to write a destructor, 
 * but I just wanted to make an example without dynamically allocating memory for once :)
 **/

#include <iostream>

class non_trivial_type
{
   private:
      int m_i;
   public:
      non_trivial_type() = default;
      non_trivial_type(const non_trivial_type&) /* "wrong" copy ctor */
      {
      }
      non_trivial_type& operator=(const non_trivial_type&) /* "wrong" copy assignment */ 
      {
         return *this;
      }

      int get_i() const
      {
         return m_i;
      }

      void initialize(int i)
      {
         m_i = i;
      }
};

class rof
{
   non_trivial_type m_type;

   public:
      rof(int i);
      rof(const rof& other) = default;            /* Copy constructor */
      rof& operator=(const rof& other) = default; /* Copy assignment */
      ~rof() = default;                           /* Destructor */

      int get() const;
};

rof::rof(int i)
{
   this->m_type.initialize(i);
}

int rof::get() const
{
   return m_type.get_i();
}

int main()
{
   rof r1(2);
   rof r2(3);

   r1 = r2;

   std::cout << r1.get() << std::endl;
   std::cout << r2.get() << std::endl;

   return 0;
}
