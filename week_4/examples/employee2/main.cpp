#include <iostream>
#include "Employee.hpp"

int main()
{
   Employee employee("Bruce Lee", 32, 1337.0);

   std::cout << employee; // get translated to the call: operator<<(std::cout, employee);
   std::cout << std::endl;

   Employee employee1("Kurt Cobain", 27, 5.0);
   Employee employee2;
   
   employee2 = employee1; // employee2 is now a copy of employee1

   std::cout << employee1 << std::endl;
   std::cout << employee2 << std::endl;

   /**
    * Ok, I don't know which company would hire Bruce Lee and two Kurt Cobains...
    **/

   return 0;
}
