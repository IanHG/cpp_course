class X
{
  private:
    int m_x; // built-in types are trivially copy-able
};

int main()
{
   X x; // default constructor is also compiler generated. why?
   X y(x); // allowed an well defined, data members of X are trivially copy-able
   X z;
   z = x; // also fine

   return 0; // here x goes out of scope is trivially destroyed
}
