#include <iostream>

class X
{
   private:
      int i_;
};

int main()
{
   X x;
   X y(x);
   X z;
   z = x;
   return 0;
}
