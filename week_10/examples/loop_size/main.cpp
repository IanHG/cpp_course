#include <iostream>
#include <vector>

int main() {
   std::vector<double> v = {1.0,2.0,3.0}; // declare and initialize vector
   for(size_t i = 0; i < v.size(); ++i) { // loop over elements
      std::cout << v[i] << std::endl; // print elements
   }
   return 0;
}
