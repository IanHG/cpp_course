#include <vector>
#include <iostream>
#include <algorithm> // for std::sort

bool is_larger_than (int i,int j) { return (i>j); } // checks if i is larger than j

int main() {
   std::vector<int> v = {3,5,1,13};
   std::sort(v.begin(), v.end(), is_larger_than); // sort v from highest to lowest
   // v=(13,5,3,1)
   
   for(int i=0; i<v.size(); ++i)
      std::cout << v[i] << " ";
}
