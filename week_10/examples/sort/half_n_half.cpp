#include <vector>
#include <iostream>
#include <algorithm> // for std::sort

bool is_larger_than (int i,int j) { return (i>j); } // checks if i is larger than j

int main() {
   std::vector<int> v = {3,5,1,13};

   std::sort(v.begin(), v.begin()+2); // sort two first elements of v ascending
   // v=(3,5,1,13)
   std::sort(v.begin()+2, v.end(), is_larger_than); // sort rest of v descending
   // v=(3,5,13,1)
}
