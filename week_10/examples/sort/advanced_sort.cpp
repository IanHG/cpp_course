// sort algorithm example
#include <iostream>     // std::cout
#include <algorithm>    // std::sort
#include <vector>       // std::vector

// comparison function
bool is_larger_than (int i,int j) { return (i>j); } // checks if i is larger than j

// comparison functor
struct is_smaller_than {
   bool operator() (int i,int j) { return (i<j);} // checks if i is smaller than j
};

int main () {
   std::vector<int> myvector {32,71,12,45,26,80,53,33}; // 32 71 12 45 26 80 53 33

   // using default comparison (operator <):
   std::sort (myvector.begin(), myvector.begin()+4);           //(12 32 45 71)26 80 53 33
                                                               
   // using function as comp
   std::sort (myvector.begin()+4, myvector.end(), is_larger_than); // 12 32 45 71(80 53 33 26)
                                                                   
   // using object as comp
   std::sort (myvector.begin(), myvector.end(), is_smaller_than());  //(12 26 32 33 45 53 71 80)
                                                                        
   // print out content:
   std::cout << "myvector contains:";
   for (std::vector<int>::iterator it=myvector.begin(); it!=myvector.end(); ++it)
      std::cout << ' ' << *it;
   std::cout << '\n';

   return 0;
}
