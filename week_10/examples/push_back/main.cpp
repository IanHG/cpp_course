#include <vector>

int main() {
   std::vector<int> v; // initilialize vector of size 0
   v.reserve(4); // allocate 4 elements. size = 0, capacity = 4
   v.push_back(2); // push_back, size = 1, capacity = 4
   v.push_back(3); // push_back, size = 2, capacity = 4
   v.push_back(4); // push_back, size = 3, capacity = 4
   v.push_back(5); // push_back, size = 4, capacity = 4
   // v = (2,3,4,5)
}
