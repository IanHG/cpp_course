#include <iostream>
#include <vector>
#include <numeric> // for std::accumulate

int multiply(int i, int j) { return i*j; }

int main() {
   std::vector<int> v {1,2,3};
   int init = 0;
   std::cout << std::accumulate(v.begin(),v.end(),init) << std::endl; // prints 6
   std::cout << std::accumulate(v.begin(),v.end(),1,multiply) << std::endl;
   // what will the above line print?
}
