// accumulate example
#include <iostream>     // std::cout
#include <functional>   // std::minus
#include <numeric>      // std::accumulate
#include <vector>       // std::vector

int myfunction (int x, int y) {return x+2*y;}

struct myclass {
   int operator()(int x, int y) {return x+3*y;}
};

int main () {
   int init = 100;
   std::vector<int> numbers = {10,20,30};

   std::cout << "using default accumulate: ";
   std::cout << std::accumulate(numbers.begin(),numbers.end(),init);
   std::cout << '\n';

   std::cout << "using functional's minus: ";
   std::cout << std::accumulate (numbers.begin(), numbers.end(), init, std::minus<int>());
   std::cout << '\n';

   std::cout << "using custom function: ";
   std::cout << std::accumulate (numbers.begin(), numbers.end(), init, myfunction);
   std::cout << '\n';

   std::cout << "using custom object: ";
   std::cout << std::accumulate (numbers.begin(), numbers.end(), init, myclass());
   std::cout << '\n';

   return 0;
}
