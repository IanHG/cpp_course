#include <iostream>

int main()
{
   int i=0;
   int* pi = new int[5];

   for(int* iter = pi; iter!=pi+5; ++iter) // loop over pointer
   {
      (*iter) = ++i;
   }
   
   for(int* iter = pi; iter!=pi+5; ++iter)
   {
      std::cout << (*iter) << std::endl; // what will get printed? :O
   }
}
