#include <iostream>
#include <vector>

int main() {
   std::vector<double> v = {1.0,2.0,3.0}; // declare and initialize vector
   for(std::vector<double>::iterator iter = v.begin(); iter != v.end(); ++iter) {
      std::cout << (*iter) << std::endl; // print elements
   }
   return 0;
}
