#include <iostream>
#include <vector>
#include <algorithm>

bool sign = true;

int alternate(int i, int j)
{
   if(sign)
   {
      sign = false;
      return i - j;
   }
   else
   {
      sign = true;
      return i + j;
   }
}

int main()
{
   std::vector<int> v { 7, 3, 2, 15, 13 };
   std::cout << std::accumulate(v.begin(), v.end(), 0, alternate) << std::endl;
}
