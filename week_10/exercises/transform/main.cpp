#include <iostream>
#include <vector>
#include <algorithm> // for std::transform

int multiply_by_two(int i)
{
   return 2*i;
}

int main()
{
   std::vector<int> v {1,2,3,4};
   std::vector<int> v2(4);

   std::transform(v.begin(),v.end(),v2.begin(),multiply_by_two);

   for(int i=0; i<v2.size(); ++i)
      std::cout << v2[i] << std::endl;
}
