#include <iostream>
#include <vector>
#include <algorithm>

bool weird(int i, int j)
{
   if(!(i%2) && j%2)
   {
      return false;
   }
   else if(i%2 && !(j%2))
   {
      return true;
   }
   else if(i%2 && j%2)
   {
      return i<j;
   }
   else 
   {
      return i>j;
   }
}

int main()
{
   std::vector<int> v { 1,2,3,4 };

   std::sort(v.begin(),v.end(),weird);

   for(int i=0; i<v.size(); ++i)
   {
      std::cout << v[i] << std::endl;
   }
}
