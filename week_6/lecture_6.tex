\documentclass[t]{beamer}
\usetheme{CambridgeUS}

\input{header.tex}
\input{codetypeset.tex}
\input{ls.tex}

\begin{document}
\title{Class Inheritance}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   \titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Today
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   {Today}
   
   Today we will cover:
   \begin{itemize}
     \item
   Class inheritance
     \item
   Base classes and derived classes
   \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Remember our \icpp{Employee} class}

   \raisebox{2mm}{%
      \includecpplines[Employee-class]{../week_3/examples/employee/Employee.hpp}{10}{28}%
   }
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Managers using composition}

   We also want managers in our corporation \\
   \verticalspace
   A \icpp{Manager} manages other \icpp{Employee}'s \\
   \verticalspace
   A \icpp{Manager} {\bf is} also an \icpp{Employee} \\
   
   \begin{cpp}[Using composition]
      class Manager
      {
        private:
          Employee m_employee; // hold the managers employee data
          std::list<Employee> m_group; // he must manage some people
          int m_level; // his level in the heirarchy
        public:
          // ... more stuff ...
      };
   \end{cpp}

   The \icpp{Manager} has an \icpp{Employee} member, to hold his employee data
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Managers using inheritance}

   We could instead inherit from the \icpp{Employee} class \\
   
   \begin{cpp}[Using inheritance]
      class Manager: public Employee // we now inherit from Employee class
      {
        private:
          std::list<Employee> m_group;
          int m_level;
        public:
          // ... more stuff ...
      };
   \end{cpp}
   
   \icpp{public} inheritance implements a {\bf is-a} relationship \\
   \verticalspace
   A \icpp{Manager} {\bf is-an} \icpp{Employee} \\
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{frame}[fragile]{\icpp{public} inheritance}
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Inheritance Tree}

   \icpp{Manager} is \emph{derived} from \icpp{Employee}, and \icpp{Employee} is a \emph{base class} for \icpp{Manager} \\ 
   \verticalspace
   We also call \icpp{Employee} the \emph{Base class} and \icpp{Manager} the \emph{Derived class} \\
   \verticalspace
   \begin{center}
      \includegraphics{figures/derivation/derivation-figure0.pdf}
   \end{center}
   \verticalspace
   Derivation/inheritance is represented by an arrow pointing from Derived- to Base- class \\
   \verticalspace
   Alternatively: some people use Super-class and Sub-class (I find it confusing, {\bf DON'T DO IT!})
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Data: Memory layout}

   In memory the derived class will have all the base class stuff, with the members specific to the derived class appended at the end \\
   \begin{figure}
      \centering
      \begin{minipage}[!t]{0.45\textwidth}
      \centering
      \raisebox{8mm}{%
      \includegraphics{figures/class_content/class_content-figure0.pdf} %
      }%
      \end{minipage}%
      \begin{minipage}[!t]{0.45\textwidth}
      \centering
         \includegraphics{figures/class_content/class_content-figure1.pdf} 
      \end{minipage}
   \end{figure}
   
   The compiler may pad each data object with additional bytes, as to ensure memory alignment of next member\\
   \verticalspace
   The size of a class may thus be larger than the sum of its parts! \\
      
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Member access}

   From Derived class \icpp{Manager}, we can access public members of Base class \icpp{Employee} \\
   
   \begin{cpp}[]
      int main()
      { 
         Manager man;
         
         man.GiveRaise(); // awesome!
         std::cout << man.Age() << std::endl; // will call Age() from Employee base, is this ok?
         std::cout << man.m_age << std::endl; // m_age is private in Employee, will this compile??...
         
         return 0;
      }
   \end{cpp}

   Calling \icpp{Employee::Age()} is allowed. It is \icpp{public}. \\
   \verticalspace
   Directly accessing \icpp{Employee::m_age} is not allowed. It is \icpp{private}.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Type relaxation}

   An \icpp{Employee*} (pointer-to \icpp{Employee}) can point to any type derived from \icpp{Employee} \\
   \verticalspace
   Similarly \icpp{Employee&} (reference-to \icpp{Employee}) can reference any type derived from \icpp{Employee} \\
   \verticalspace
   In both cases only \icpp{Employee} interface can be accessed
   
   \begin{cpp}
      Manager man;
      
      Employee* pemp = &man; 
      Employee& remp = man;
      Employee emp = man; // beware this will also compile, 
                          // will copy Employee data from Manager
   \end{cpp}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Pass \icpp{Manager} to function taking \icpp{Employee}}

   I can call functions taking a reference to \icpp{Employee} with a \icpp{Manager} object \\
   \verticalspace
   Argument type will be relaxed using the type relaxation rules

   \includecpplines[]{examples/manager/main.cpp}{6}{13}

   \begin{cpp}
      Manager man;
      GiveRaiseToEmployee(man);
   \end{cpp}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\icpp{Manager} example functions}

   We define a \icpp{public} function \icpp{GoodManagement} in class \icpp{Manager} \\

   \begin{cpp}
      class Manager: public Employee
      {
         // ... some stuff ... 
         public:
            Manager(const Manager& manager); // declare copy constructor      

            void GoodManagement(); // this function can access public members of Employee
      };
   \end{cpp}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {\icpp{Manager} constructors}

   When constructing \icpp{Manager} we call constructor of \icpp{Employee} \\
   \verticalspace
   When copy constructing \icpp{Manager} we pass the reference to \icpp{Manager} object we are copying to the copy constructor of \icpp{Employee} \\

   \begin{cpp}
      class Manager: public Employee
      {
        public:
          Manager(const Manager& man)
            :  Employee(man) // call Employee's copy c-tor
            ,  m_group(man.m_group)
            ,  m_level(man.m_level)
          {
          }
      };
   \end{cpp}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Implementation of \icpp{GoodManagement()}}
   
   Members of \icpp{Manager} can only access the \icpp{public} members of \icpp{Employee} \\
   \verticalspace

   \includecpplines[]{examples/manager/Manager.hpp}{30}{34}

   \includecpplines[]{examples/manager/Manager.hpp}{45}{49}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   {\icpp{private} members of \icpp{Employee}}

   In \icpp{Manager} we cannot access \icpp{private} members of \icpp{Employee}, they are still \icpp{private} to \icpp{Employee} \\

   \includecpplines[This is not allowed!]{examples/manager/Manager.hpp}{37}{41}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   {\icpp{protected} specifier/keyword}
   
   \icpp{protected} members are accessible by derived classes, but not outside

   \includecpplines[]{examples/protected_specifier/main.cpp}{3}{11}
   
   \includecpplines[]{examples/protected_specifier/main.cpp}{17}{18}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   {Overview table}

   Access to members of a class depending on access-speficier.
   \verticalspace

   \begin{center}
      \begin{tabular}{@{}l|lll@{}}
         Access            &  \icpp{public}  &  \icpp{protected}  &  \icpp{private}  \\ \hline
         Same class        &  yes            &  yes               &  yes      \\
         Derived classes   &  yes            &  yes               &  no       \\
         Outside classes   &  yes            &  no                &  no       \\
      \end{tabular}
   \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Access to members in \icpp{public} inheritance}
   
   \begin{center}
      \icpp{class ClassA: public ClassB}
   \end{center}
   
   When inheriting \icpp{public}'ly:
   \begin{itemize}
      \item
         All \icpp{public} members of a base class becomes \icpp{public} members of derived class \\
      \item
         All \icpp{protected} members of a base class becomes \icpp{protected} members of derived class \\
      \item
         All \icpp{private} members of a base class stay \icpp{private} and cannot be accessed outside of the base class \\
      \item
         A member function of a derived class can use the \icpp{public} and \icpp{protected} members of a base class \\
      \item
         Outside code can call \icpp{public} members of a base class \\
   \end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Access to members in \icpp{protected} inheritance}
   
   \begin{center}
      \icpp{class ClassA: protected ClassB}
   \end{center}
   
   When inheriting \icpp{protected}'ly (?):
   \begin{itemize}
      \item
         All \icpp{public} members of a base class becomes \icpp{protected} members of derived class \\
      \item
         All \icpp{protected} members of a base class becomes \icpp{protected} members of derived class \\
      \item
         All \icpp{private} members of a base class stay \icpp{private} and cannot be accessed outside of the base class \\
      \item
         A member function of a derived class can use the \icpp{public} and \icpp{protected} members of a base class \\
      \item
         Outside code cannot call \icpp{public} members of a base class \\
   \end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
   {Access to members in \icpp{private} inheritance}

   \begin{center}
      \icpp{class ClassA: private ClassB}
   \end{center}
   
   When inheriting \icpp{private}'ly:
   \begin{itemize}
      \item
         All \icpp{public} members of a base class becomes \icpp{private} members of derived class \\
      \item
         All \icpp{protected} members of a base class becomes \icpp{private} members of derived class \\
      \item
         All \icpp{private} members of a base class stay \icpp{private} and cannot be accessed outside of the base class \\
      \item
         A member function of a derived class can use the \icpp{public} and \icpp{protected} members of a base class \\
      \item
         Outside code cannot call \icpp{public} members of a base class \\
   \end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
   {Ups and downs}

   You will mostly do \icpp{public} and \icpp{private} inheritance. \\
   \verticalspace
   \icpp{protected} inheritance is less applicable (I have used it once, I think) \\
   \verticalspace
   \verticalspace

   Inheritance pros:
   \begin{itemize}
      \item
         Code can be heirarchically organized in a way where implementation somewhat mirrors the real world.
      \item
         Re-use of old code.
   \end{itemize}
   Inheritance cons:
   \begin{itemize}
      \item
         Can quickly become a giant mess with very intricate heirarchies!
   \end{itemize}

   \verticalspace
   Depending on {\bf what} you do and {\bf who} you ask, will make code \emph{easier/harder} to read

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{And thats a wrap!}
   \begin{center}
      \Large
      Lets get to some coding!
   \end{center}
\end{frame}

\end{document}
