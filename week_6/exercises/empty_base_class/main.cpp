#include <iostream>

/**
 * For more information:
 *
 *    https://en.cppreference.com/w/cpp/language/ebo
 *
 **/

class EmptyBase
{
};

class Derived: public EmptyBase
{
   int m_i;
};

class Composed
{
   EmptyBase m_base;
   int       m_i;
};

int main()
{
   std::cout << sizeof(EmptyBase) << std::endl;
   std::cout << sizeof(Derived)   << std::endl;
   std::cout << sizeof(Composed)  << std::endl;

   return 0;
}
