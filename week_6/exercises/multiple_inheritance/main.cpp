class A
{
};

class B
   :  public A
{
};

class C
{
};

class D
   :  private B
{
};

class E
   :  public C
   ,  public D
{
};

int main()
{
}
