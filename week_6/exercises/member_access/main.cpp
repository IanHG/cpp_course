#include <iostream>

class Base
{
   private:
      void base_private() {}
   protected:
      void base_protecte() {}
   public:
      void base_public() {}
};

class Derived  :  public Base
{
   private:
      void derived_private() {}
   protected:
      void derived_protected() {}
   public:
      void derived_public() {}
};

int main()
{
}
