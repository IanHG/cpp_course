\documentclass[]{article}

\input{header.tex}
\input{codetypeset.tex}

\begin{document}
\normalsize
\begin{center}
\bf\large Exercises Week 6
\end{center}
\begin{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf Relationship}

  What type of relationship does \icpp{public} inheritance implement?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf This \emph{is-an} exercise}

  List $5-10$ pairs of things that has an {\bf is-a} -relationship.

  \emph{Extra:} You can also go beyond pairs, \emph{e.g.} A Horse is-a Mammal is-an Animal. Just go crazy!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf Management}

  Go to \iterminal{examples/manager}. 
  Try commenting in each version of \icpp{GoodManagement()} in \icpp{Manager.h} and compile the program.
  Make sure you know what happens in each case, and why.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf Points}
  
   Implement a \icpp{Point2D} class by inheriting the 1D functionalities from the following \icpp{Point1D} class.

   \includecpplines[1D point]{points/Point1D.h}{4}{21}
   
   Implement a \icpp{Point3D} class by inheriting 2D behaviour from \icpp{Point2D}.

   Make funtions to move and print the points.

   Draw an inheritance tree for your class hierarchy (on paper :O! ).

   \emph{N.B.:} This exercise is just to get you to implement an easy class heirarchy. I do not advocate implementing your 1D, 2D, and 3D points like this in general.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\item{\bf Members only!}

   For each member function in \icpp{Base} and \icpp{Derived} list from where it is accessible. 
   Options are:
   \begin{center}
      inside \icpp{Base}, inside \icpp{Derived}, and outside both classes
   \end{center}

      \includecpplines[]{member_access/main.cpp}{3}{21}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf An \icpp{Employee} is-(also)-a \icpp{Person}}

   Take the \icpp{Employee}/\icpp{Manager} example and implement the relevant parts of \icpp{Employee} using a more general base class \icpp{Person}.

   Implement a function to celebrate a \icpp{Person}'s birthday. Remember this should increase the age of the \icpp{Person}. Create an \icpp{Employee} and celebrate his birthday.

   Implement a class \icpp{ChuckNorris} (as Chuck Norris is in a class of his own!) using the general base \icpp{Person} (arguably Chuck Norris is a Diety, not a mere person, but for the sake of this exercise we will forgo this fact (Fact: "Chuck Norris doesn't need a debugger. He just stares down the bug until the code confesses!")).

   Celebrate \icpp{ChuckNorris}'s birthday.

   \emph{Extra:} Make a \icpp{Diety} base class and make \icpp{ChuckNorris} also be a Diety using multiple inheritance.

   \emph{Extra:} Draw an inheritance tree for your \icpp{Person}/\icpp{Diety} class heirarchy.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf \icpp{sizeof} non-POD types}
   
   \emph{N.B:} Some parts of this exercise are a little technical, but, good to know.

   In \cppname{} the \icpp{sizeof} operator returns the size, in bytes, of a type or object.

   Use your \emph{Google-fu} to find common sizes of \cppname{} types and make an educated guess on what will get printed in the following:

   \includecpplines[POD]{sizeof/pod.cpp}{5}{10}

   The \icpp{alignof} operator returns the bit boundary alignment of a type. 
   Try to compile and run \iterminal{sizeof/pod_alignof.cpp}.
   What do you notice about the POD types?
   The alignment of non-POD types will be equal to alignment of the class member with the largest alignment requirement.
   
   Using your knowledge on class object memory layout, make an educated guess on what the sizes of the following types are.
   
   \emph{N.B:} As the padding of class types is in principle compiler depedent, 
   you might not get the expected result for the following sizes (most compilers will likely pad in the same way though).

   \includecpplines[Non-POD]{sizeof/non_pod.cpp}{3}{29}

   Rationalize whether you got the expected result or not.
   An easy way to make sure you have the least padding in your \icpp{struct}'s,
   is to declare member data in an order of decreasing size.
   This way padding will be minimized, and your \icpp{struct} will
   be as small as it can possibly be (without disabling padding).

   \hiddenhint{See \emph{e.g.} this \href{https://www.includehelp.com/cpp-tutorial/size-of-a-class-in-cpp-padding-alignment-in-class-size-of-derived-class.aspx}{description} of class memory layout.}

   To see the memory layout of each class in more detailed manner you can also use the tool \iterminal{pahole} (Poke-A-Hole).
   \iterminal{pahole} needs the executable to be compiled with debug information.
   Try installing \iterminal{pahole} and run the executable through it, 
   to see whether your expectations line up with what is printed by \iterminal{pahole}.

   \begin{terminal}
      $ g++ -g non_pod.cpp
      $ pahole --class_name=A,B,C,D,E a.out
   \end{terminal}

   \iterminal{pahole} is part of the \iterminal{dwarves} package, and can likely be installed with your systems package manager,
   or downloaded from here \href{https://github.com/acmel/dwarves}{https://github.com/acmel/dwarves}.
   If you cannot install or get \iterminal{pahole} to work, 
   look at the output located in \iterminal{sizeof/pahole} directory instead.

   If you want your \icpp{struct}'s and \icpp{class}'es to have the minimum possible size,
   you should declare member variables in order of descending alignment boundary
   (for POD types this corresponds also to their size in bytes).
   This will ensure the least amount of padding. What are the sizes of these two \icpp{struct}'s?
   
   \clearpage
   \includecpplines[Minimal size]{sizeof/minimal_size.cpp}{3}{23}
   
   Try to compile the file, run the code, and run it through \iterminal{pahole}.

   Size is, however, not the only concern when ordering member variables, 
   one should also think about how the \icpp{struct} is read into cache.
   Members that are used together should be close to each-other in memory,
   and preferably on the same cache-line.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf \emph{Empty Base-class Optimization} }

   \emph{N.B:} This exercise is a little technical, but, good to know.

   In order to guarantee that the address of distinct objects are distinct,
   the size of an object will be at least 1,
   even if the type is empty.

   If an empty type is a member of a another type / \icpp{class}, 
   its size will be at least 1 plus padding to ensure the memory boundary alignment of the next element.

   However, this does not apply to empty base classes, which can be optimized out of the objects memory layout, hence the name \emph{Empty Base-class Optimization}.

   The size of an \icpp{int} is \icpp{4} bytes, and will be aligned on a \icpp{4} byte boundary (\emph{i.e.} the address of the \icpp{int} object modulo \icpp{4} is \icpp{0}).

   With these facts in mind what does the following program print (if you are completely lost, run the program and rationalize based on the output instead)?

   \includecpp[]{empty_base_class/main.cpp}

   \emph{Extra:} What happens if \icpp{EmptyBase} has a member function, but still no data members?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\item{\bf \icpp{class ProjectEuler: public Homepage} }

  Go do some problems. 
  
  This weeks challenge: in each problem make a class that inherits from another class.

\end{enumerate} 

\end{document}
