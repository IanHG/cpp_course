#ifndef POINT1D_H_INCLUDED
#define POINT1D_H_INCLUDED

class Point1D
{
   private:
      double m_x;
   public:
      Point1D(double x): m_x(x)
      {
      }

      Point1D(const Point1D& p): m_x(p.m_x)
      {
      }

      void move(double x)
      {
         m_x = x;
      }
};

#endif /* POINT1D_H_INCLUDED */
