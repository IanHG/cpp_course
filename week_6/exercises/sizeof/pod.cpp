#include <iostream>

int main()
{
   std::cout << "bool    : "  << sizeof(bool)     << std::endl;
   std::cout << "char    : "  << sizeof(char)     << std::endl;
   std::cout << "int     : "  << sizeof(int)      << std::endl;
   std::cout << "long int: "  << sizeof(long int) << std::endl;
   std::cout << "float   : "  << sizeof(float)    << std::endl;
   std::cout << "double  : "  << sizeof(double)   << std::endl;

   return 0;
}
