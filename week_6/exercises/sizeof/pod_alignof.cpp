#include <iostream>

int main()
{
   std::cout << "bool    : "  << sizeof(bool)     << "  " << alignof(bool)     << std::endl;
   std::cout << "char    : "  << sizeof(char)     << "  " << alignof(char)     << std::endl;
   std::cout << "int     : "  << sizeof(int)      << "  " << alignof(int)      << std::endl;
   std::cout << "long int: "  << sizeof(long int) << "  " << alignof(long int) << std::endl;
   std::cout << "float   : "  << sizeof(float)    << "  " << alignof(float)    << std::endl;
   std::cout << "double  : "  << sizeof(double)   << "  " << alignof(double)   << std::endl;

   return 0;
}
