#include <iostream>

class A
{
   int i;
};

class B
{
   char   c[3];
   double d;
};

class C: public A
{
   bool b;
};

class D: public A, public B
{
   float* pf;
   long int li;
};

class E
{
   double d[2];
   char   c;
};

int main()
{
   // Print sizes of classes
   std::cout << "A: " << sizeof(A) << "  " << alignof(A) << std::endl;
   std::cout << "B: " << sizeof(B) << "  " << alignof(B) << std::endl;
   std::cout << "C: " << sizeof(C) << "  " << alignof(C) << std::endl;
   std::cout << "D: " << sizeof(D) << "  " << alignof(D) << std::endl;
   std::cout << "E: " << sizeof(E) << "  " << alignof(E) << std::endl;
   
   // Need to declare some "dummy" variables for 'pahole' tool
   A a;
   B b;
   C c;
   D d;
   E e;

   return 0;
}
