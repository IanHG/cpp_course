#include <iostream>

struct data_A
{
   int    i1;
   double d1;
   char   c1[2];
   float  f1;
   bool   b1;
   double d2;
   char   c2;
};

struct data_B
{
   double d1;
   double d2;
   float  f1;
   int    i1;
   char   c1[2];
   char   c2;
   bool   b1;
};

int main()
{
   // Print sizes of classes
   std::cout << "data_A: " << sizeof(data_A) << "  " << alignof(data_A) << std::endl;
   std::cout << "data_B: " << sizeof(data_B) << "  " << alignof(data_B) << std::endl;
   
   // Need to declare some "dummy" variables for 'pahole' tool
   data_A a;
   data_B b;

   return 0;
}
