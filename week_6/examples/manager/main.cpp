#include <iostream>

#include "Employee.hpp"
#include "Manager.hpp"

void GiveRaiseToEmployee(Employee& employee)
{
   std::cout   << " Hello : " << employee.Name() 
               << ". You have been given a raise! :)"
               << std::endl; 

   employee.GiveRaise();
}

int main()
{
   Manager man("Billy-Bob", 57, 101234123.2);

   Employee* pemp = &man;
   Employee& remp = man;
   Employee emp   = man;
   
   man.GoodManagement();

   GiveRaiseToEmployee(man);

   return 0;
}
