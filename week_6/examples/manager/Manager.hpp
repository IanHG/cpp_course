#ifndef MANAGER_H_INCLUDED
#define MANAGER_H_INCLUDED

#include <list>
#include "Employee.hpp"

/**
 * Manager that inherits from Employee
 **/
class Manager: public Employee
{
   std::list<Employee> m_group;
   int                 m_level;

   public:
      Manager() = default; // default c-tor
      Manager(const std::string& name, int age, double salary)
         :  Employee(name, age, salary)
         ,  m_group()
         ,  m_level(0)
      {
      }
      Manager(const Manager& man)
         :  Employee(man) // call employee's copy ctor
         ,  m_group(man.m_group)
         ,  m_level(man.m_level)
      {
      }

      void GoodManagement()
      {
         // ... manipulate group of employees ...
         GiveRaise(); // OK, GiveRaise() is public in Employee
      }
      
      /*
      void GoodManagement()
      {
         // ... manipulate group of employees ...
         m_salary *= 10; // m_salary is private in Employee... COMPILATION ERROR!
      }
      */
      
      /*
      void GoodManagement()
      {
         // ... manipulate group of employees ...
         Employee::GiveRaise(); // We specifically say that GiveRaise() is a member of Employee, OK!
      }
      */
};

#endif /* MANAGER_H_INCLUDED */
