#pragma once                  // Header guards
#ifndef EMPLOYEE_HPP_INCLUDED
#define EMPLOYEE_HPP_INCLUDED

#include <string> // include for std::string

/**
 * Define class Employee
 **/
class Employee
{
   private: 
      /* Data members */
      std::string m_name; // Name of employee
      int         m_age;  // The age of employee
      double      m_salary; // The salary the employee gets

   public:
      Employee(); /* Default constructor */
      Employee(const Employee& other); /* Copy contructor */
      Employee(const std::string& name, int age, double salary); /* Custom constructor */
      ~Employee(); /* Destructor */
      
      void GiveRaise(); /* Member function */
      int Age() const;  /* Member function */
      const std::string& Name() const; /* Member function */

      friend bool operator==(const Employee& e1, const Employee& e2); /* Friend */
};

#endif /* EMPLOYEE_HPP_INCLUDED */
