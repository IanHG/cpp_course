#include <iostream>

class A
{
   protected:  int m_i = 1;
};

class B  :  public A
{
   public:  int get_i() { return m_i; }
};

int main()
{
   B b;

   std::cout << b.get_i() << std::endl;
   //std::cout << b.m_i     << std::endl;  // Not allowed, will not compile

   return 0;
}
